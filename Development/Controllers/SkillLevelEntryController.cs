﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Development.Models;

namespace Development.Controllers
{
    public partial class SkillLevelEntryController : Controller
    {
        private DevelopmentContext db = new DevelopmentContext();

        //
        // GET: /SkillLevelEntry/

        public virtual ViewResult Index()
        {
            var skilllevelentries = db.SkillLevelEntries.Include(s => s.skill).Include(s => s.skillLevel);
            return View(skilllevelentries.ToList());
        }

        //
        // GET: /SkillLevelEntry/Details/5

        public virtual ViewResult Details(int id)
        {
            SkillLevelEntry skilllevelentry = db.SkillLevelEntries.Find(id);
            return View(skilllevelentry);
        }

        //
        // GET: /SkillLevelEntry/Create

        public virtual ActionResult Create()
        {
            ViewBag.skillId = new SelectList(db.Skills, "id", "name");
            ViewBag.skillLevelId = new SelectList(db.SkillLevels, "id", "name");
            return View();
        } 

        //
        // POST: /SkillLevelEntry/Create

        [HttpPost]
        public virtual ActionResult Create(SkillLevelEntry skilllevelentry)
        {
            if (ModelState.IsValid)
            {
                db.SkillLevelEntries.Add(skilllevelentry);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.skillId = new SelectList(db.Skills, "id", "name", skilllevelentry.skillId);
            ViewBag.skillLevelId = new SelectList(db.SkillLevels, "id", "name", skilllevelentry.skillLevelId);
            return View(skilllevelentry);
        }
        
        //
        // GET: /SkillLevelEntry/Edit/5

        public virtual ActionResult Edit(int id)
        {
            SkillLevelEntry skilllevelentry = db.SkillLevelEntries.Find(id);
            ViewBag.skillId = new SelectList(db.Skills, "id", "name", skilllevelentry.skillId);
            ViewBag.skillLevelId = new SelectList(db.SkillLevels, "id", "name", skilllevelentry.skillLevelId);
            return View(skilllevelentry);
        }

        //
        // POST: /SkillLevelEntry/Edit/5

        [HttpPost]
        public virtual ActionResult Edit(SkillLevelEntry skilllevelentry)
        {
            if (ModelState.IsValid)
            {
                db.Entry(skilllevelentry).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.skillId = new SelectList(db.Skills, "id", "name", skilllevelentry.skillId);
            ViewBag.skillLevelId = new SelectList(db.SkillLevels, "id", "name", skilllevelentry.skillLevelId);
            return View(skilllevelentry);
        }

        //
        // GET: /SkillLevelEntry/Delete/5

        public virtual ActionResult Delete(int id)
        {
            SkillLevelEntry skilllevelentry = db.SkillLevelEntries.Find(id);
            return View(skilllevelentry);
        }

        //
        // POST: /SkillLevelEntry/Delete/5

        [HttpPost, ActionName("Delete")]
        public virtual ActionResult DeleteConfirmed(int id)
        {            
            SkillLevelEntry skilllevelentry = db.SkillLevelEntries.Find(id);
            db.SkillLevelEntries.Remove(skilllevelentry);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}