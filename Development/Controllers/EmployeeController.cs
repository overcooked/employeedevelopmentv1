﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Development.Models;

namespace Development.Controllers
{
    public partial class EmployeeController : Controller
    {
        private DevelopmentContext db = new DevelopmentContext();

        //
        // GET: /Employee/

        public virtual ViewResult Index()
        {
            var employees = db.Employees.Include(e => e.defaultProject).Include(e => e.employeePosition).Include(e => e.supervisor);
            return View(employees.ToList());
        }

        //
        // GET: /Employee/Details/5

        public virtual ViewResult Details(int id)
        {
            Employee employee = db.Employees.Find(id);
            return View(employee);
        }

        //
        // GET: /Employee/Create

        public virtual ActionResult Create()
        {
            ViewBag.defaultProjectId = new SelectList(db.Projects, "id", "name");
            ViewBag.employeePositionId = new SelectList(db.EmployeePositions, "id", "name");
            ViewBag.supervisorId = new SelectList(db.Employees, "id", "firstName");
            return View();
        } 

        //
        // POST: /Employee/Create

        [HttpPost]
        public virtual ActionResult Create(Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Employees.Add(employee);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.defaultProjectId = new SelectList(db.Projects, "id", "name", employee.defaultProjectId);
            ViewBag.employeePositionId = new SelectList(db.EmployeePositions, "id", "name", employee.employeePositionId);
            ViewBag.supervisorId = new SelectList(db.Employees, "id", "firstName", employee.supervisorId);
            return View(employee);
        }
        
        //
        // GET: /Employee/Edit/5

        public virtual ActionResult Edit(int id)
        {
            Employee employee = db.Employees.Find(id);
            ViewBag.defaultProjectId = new SelectList(db.Projects, "id", "name", employee.defaultProjectId);
            ViewBag.employeePositionId = new SelectList(db.EmployeePositions, "id", "name", employee.employeePositionId);
            ViewBag.supervisorId = new SelectList(db.Employees, "id", "firstName", employee.supervisorId);
            return View(employee);
        }

        //
        // POST: /Employee/Edit/5

        [HttpPost]
        public virtual ActionResult Edit(Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.defaultProjectId = new SelectList(db.Projects, "id", "name", employee.defaultProjectId);
            ViewBag.employeePositionId = new SelectList(db.EmployeePositions, "id", "name", employee.employeePositionId);
            ViewBag.supervisorId = new SelectList(db.Employees, "id", "firstName", employee.supervisorId);
            return View(employee);
        }

        //
        // GET: /Employee/Delete/5

        public virtual ActionResult Delete(int id)
        {
            Employee employee = db.Employees.Find(id);
            return View(employee);
        }

        //
        // POST: /Employee/Delete/5

        [HttpPost, ActionName("Delete")]
        public virtual ActionResult DeleteConfirmed(int id)
        {            
            Employee employee = db.Employees.Find(id);
            db.Employees.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}