﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Development.Models;

namespace Development.Controllers
{
    public partial class LogCRUDController : Controller
    {
        private DevelopmentContext db = new DevelopmentContext();

        //
        // GET: /LogCRUD/

        public virtual ViewResult Index()
        {
            var logs = db.Logs.Include(l => l.employee).Include(l => l.skillLevel).Include(l => l.skill).Include(l => l.verifiedBy);
            return View(logs.ToList());
        }

        //
        // GET: /LogCRUD/Details/5

        public virtual ViewResult Details(int id)
        {
            Log log = db.Logs.Find(id);
            return View(log);
        }

        //
        // GET: /LogCRUD/Create

        public virtual ActionResult Create()
        {
            ViewBag.employeeId = new SelectList(db.Employees, "id", "firstName");
            ViewBag.skillLevelId = new SelectList(db.SkillLevels, "id", "name");
            ViewBag.skillId = new SelectList(db.Skills, "id", "name");
            ViewBag.verifiedById = new SelectList(db.Employees, "id", "firstName");
            return View();
        } 

        //
        // POST: /LogCRUD/Create

        [HttpPost]
        public virtual ActionResult Create(Log log)
        {
            if (ModelState.IsValid)
            {
                db.Logs.Add(log);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.employeeId = new SelectList(db.Employees, "id", "firstName", log.employeeId);
            ViewBag.skillLevelId = new SelectList(db.SkillLevels, "id", "name", log.skillLevelId);
            ViewBag.skillId = new SelectList(db.Skills, "id", "name", log.skillId);
            ViewBag.verifiedById = new SelectList(db.Employees, "id", "firstName", log.verifiedById);
            return View(log);
        }
        
        //
        // GET: /LogCRUD/Edit/5

        public virtual ActionResult Edit(int id)
        {
            Log log = db.Logs.Find(id);
            ViewBag.employeeId = new SelectList(db.Employees, "id", "firstName", log.employeeId);
            ViewBag.skillLevelId = new SelectList(db.SkillLevels, "id", "name", log.skillLevelId);
            ViewBag.skillId = new SelectList(db.Skills, "id", "name", log.skillId);
            ViewBag.verifiedById = new SelectList(db.Employees, "id", "firstName", log.verifiedById);
            return View(log);
        }

        //
        // POST: /LogCRUD/Edit/5

        [HttpPost]
        public virtual ActionResult Edit(Log log)
        {
            if (ModelState.IsValid)
            {
                db.Entry(log).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.employeeId = new SelectList(db.Employees, "id", "firstName", log.employeeId);
            ViewBag.skillLevelId = new SelectList(db.SkillLevels, "id", "name", log.skillLevelId);
            ViewBag.skillId = new SelectList(db.Skills, "id", "name", log.skillId);
            ViewBag.verifiedById = new SelectList(db.Employees, "id", "firstName", log.verifiedById);
            return View(log);
        }

        //
        // GET: /LogCRUD/Delete/5

        public virtual ActionResult Delete(int id)
        {
            Log log = db.Logs.Find(id);
            return View(log);
        }

        //
        // POST: /LogCRUD/Delete/5

        [HttpPost, ActionName("Delete")]
        public virtual ActionResult DeleteConfirmed(int id)
        {            
            Log log = db.Logs.Find(id);
            db.Logs.Remove(log);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}