﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Development.Models;

namespace Development.Controllers
{
    public partial class SkillLevelController : Controller
    {
        private DevelopmentContext db = new DevelopmentContext();

        //
        // GET: /SkillLevel/

        public virtual ViewResult Index()
        {
            return View(db.SkillLevels.ToList());
        }

        //
        // GET: /SkillLevel/Details/5

        public virtual ViewResult Details(int id)
        {
            SkillLevel skilllevel = db.SkillLevels.Find(id);
            return View(skilllevel);
        }

        //
        // GET: /SkillLevel/Create

        public virtual ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /SkillLevel/Create

        [HttpPost]
        public virtual ActionResult Create(SkillLevel skilllevel)
        {
            if (ModelState.IsValid)
            {
                db.SkillLevels.Add(skilllevel);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(skilllevel);
        }
        
        //
        // GET: /SkillLevel/Edit/5

        public virtual ActionResult Edit(int id)
        {
            SkillLevel skilllevel = db.SkillLevels.Find(id);
            return View(skilllevel);
        }

        //
        // POST: /SkillLevel/Edit/5

        [HttpPost]
        public virtual ActionResult Edit(SkillLevel skilllevel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(skilllevel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(skilllevel);
        }

        //
        // GET: /SkillLevel/Delete/5

        public virtual ActionResult Delete(int id)
        {
            SkillLevel skilllevel = db.SkillLevels.Find(id);
            return View(skilllevel);
        }

        //
        // POST: /SkillLevel/Delete/5

        [HttpPost, ActionName("Delete")]
        public virtual ActionResult DeleteConfirmed(int id)
        {            
            SkillLevel skilllevel = db.SkillLevels.Find(id);
            db.SkillLevels.Remove(skilllevel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}