﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Development.Models;

namespace Development.Controllers
{
    public partial class VerificationController : Controller
    {
        private DevelopmentContext db = new DevelopmentContext();

        //
        // GET: /Verification/

        public virtual ViewResult Index()
        {
            var verifications = db.Verifications.Include(v => v.task).Include(v => v.verifiedBy);
            return View(verifications.ToList());
        }

        //
        // GET: /Verification/Details/5

        public virtual ViewResult Details(int id)
        {
            Verification verification = db.Verifications.Find(id);
            return View(verification);
        }

        //
        // GET: /Verification/Create

        public virtual ActionResult Create()
        {
            ViewBag.taskId = new SelectList(db.Tasks, "id", "notes");
            ViewBag.verifiedById = new SelectList(db.Employees, "id", "firstName");
            return View();
        } 

        //
        // POST: /Verification/Create

        [HttpPost]
        public virtual ActionResult Create(Verification verification)
        {
            if (ModelState.IsValid)
            {
                db.Verifications.Add(verification);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.taskId = new SelectList(db.Tasks, "id", "notes", verification.taskId);
            ViewBag.verifiedById = new SelectList(db.Employees, "id", "firstName", verification.verifiedById);
            return View(verification);
        }
        
        //
        // GET: /Verification/Edit/5

        public virtual ActionResult Edit(int id)
        {
            Verification verification = db.Verifications.Find(id);
            ViewBag.taskId = new SelectList(db.Tasks, "id", "notes", verification.taskId);
            ViewBag.verifiedById = new SelectList(db.Employees, "id", "firstName", verification.verifiedById);
            return View(verification);
        }

        //
        // POST: /Verification/Edit/5

        [HttpPost]
        public virtual ActionResult Edit(Verification verification)
        {
            if (ModelState.IsValid)
            {
                db.Entry(verification).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.taskId = new SelectList(db.Tasks, "id", "notes", verification.taskId);
            ViewBag.verifiedById = new SelectList(db.Employees, "id", "firstName", verification.verifiedById);
            return View(verification);
        }

        //
        // GET: /Verification/Delete/5

        public virtual ActionResult Delete(int id)
        {
            Verification verification = db.Verifications.Find(id);
            return View(verification);
        }

        //
        // POST: /Verification/Delete/5

        [HttpPost, ActionName("Delete")]
        public virtual ActionResult DeleteConfirmed(int id)
        {            
            Verification verification = db.Verifications.Find(id);
            db.Verifications.Remove(verification);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}