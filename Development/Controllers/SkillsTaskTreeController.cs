﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Development.Models;
using Development.ViewModels;

namespace Development.Controllers
{
    public class SkillsTaskTreeController : Controller
    {
        private DevelopmentContext db = new DevelopmentContext();

        //
        // GET: /SkillsTaskTree/

        public ActionResult Index()
        {
            // create list to use to build tree structure 
            var skillsTree = new List<Skill>();

            // call recursive method to build skills tree
            BuildSkillsTree(db.Skills.ToList(), ref skillsTree, null);

            // set up view model
            var viewModel = BuildModel(skillsTree);


            return View(viewModel);
        }


        #region helper methods
        [NonAction]
        public void BuildSkillsTree(List<Skill> skills, ref List<Skill> treeOrderedSkills, int? parentID)
        {
            // Get any children of parentID passed in
            var childSkills = skills.Where(n => n.parentId == parentID).OrderBy(n => n.orderId).ToList();

            // recursively add child skills
            foreach (var s in childSkills)
            {
                treeOrderedSkills.Add(s);
                BuildSkillsTree(skills, ref treeOrderedSkills, s.id);
            }
        }


        [NonAction]
        public SkillsTaskTreeModel BuildModel(List<Skill> tree)
        {
            SkillsTaskTreeModel model = new SkillsTaskTreeModel
            {
                SkillTasksTree = new List<SkillTasksModel>()
            };

            // loop through skills tree and add any tasks
            foreach (var skill in tree)
            {
                var skillTasks = new SkillTasksModel
                {
                    SkillNode = skill,
                    SkillTasks = db.Tasks.Where(t => t.skillId == skill.id).ToList()
                };

                model.SkillTasksTree.Add(skillTasks);
            }

            // use skills tree to create drop down
            model.SkillsSelector = new SelectList(tree, "id", "indentedName");

            return model;
        }
        #endregion helper methods


    }
}

