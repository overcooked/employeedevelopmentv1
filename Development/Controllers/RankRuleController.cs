﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Development.Models;

namespace Development.Controllers
{
    public partial class RankRuleController : Controller
    {
        private DevelopmentContext db = new DevelopmentContext();

        //
        // GET: /RankRule/

        public virtual ViewResult Index()
        {
            var rankrules = db.RankRules.Include(r => r.skillLevel).Include(r => r.rank).Include(r => r.skill);
            return View(rankrules.ToList());
        }

        //
        // GET: /RankRule/Details/5

        public virtual ViewResult Details(int id)
        {
            RankRule rankrule = db.RankRules.Find(id);
            return View(rankrule);
        }

        //
        // GET: /RankRule/Create

        public virtual ActionResult Create()
        {
            ViewBag.skillLevelId = new SelectList(db.SkillLevels, "id", "name");
            ViewBag.rankId = new SelectList(db.Ranks, "id", "name");
            ViewBag.skillId = new SelectList(db.Skills, "id", "name");
            return View();
        } 

        //
        // POST: /RankRule/Create

        [HttpPost]
        public virtual ActionResult Create(RankRule rankrule)
        {
            if (ModelState.IsValid)
            {
                db.RankRules.Add(rankrule);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.skillLevelId = new SelectList(db.SkillLevels, "id", "name", rankrule.skillLevelId);
            ViewBag.rankId = new SelectList(db.Ranks, "id", "name", rankrule.rankId);
            ViewBag.skillId = new SelectList(db.Skills, "id", "name", rankrule.skillId);
            return View(rankrule);
        }
        
        //
        // GET: /RankRule/Edit/5

        public virtual ActionResult Edit(int id)
        {
            RankRule rankrule = db.RankRules.Find(id);
            ViewBag.skillLevelId = new SelectList(db.SkillLevels, "id", "name", rankrule.skillLevelId);
            ViewBag.rankId = new SelectList(db.Ranks, "id", "name", rankrule.rankId);
            ViewBag.skillId = new SelectList(db.Skills, "id", "name", rankrule.skillId);
            return View(rankrule);
        }

        //
        // POST: /RankRule/Edit/5

        [HttpPost]
        public virtual ActionResult Edit(RankRule rankrule)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rankrule).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.skillLevelId = new SelectList(db.SkillLevels, "id", "name", rankrule.skillLevelId);
            ViewBag.rankId = new SelectList(db.Ranks, "id", "name", rankrule.rankId);
            ViewBag.skillId = new SelectList(db.Skills, "id", "name", rankrule.skillId);
            return View(rankrule);
        }

        //
        // GET: /RankRule/Delete/5

        public virtual ActionResult Delete(int id)
        {
            RankRule rankrule = db.RankRules.Find(id);
            return View(rankrule);
        }

        //
        // POST: /RankRule/Delete/5

        [HttpPost, ActionName("Delete")]
        public virtual ActionResult DeleteConfirmed(int id)
        {            
            RankRule rankrule = db.RankRules.Find(id);
            db.RankRules.Remove(rankrule);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}