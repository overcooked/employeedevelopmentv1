﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Development.Models;

namespace Development.Controllers
{
    public partial class TaskController : Controller
    {
        private DevelopmentContext db = new DevelopmentContext();

        //
        // GET: /Task/

        public virtual ViewResult Index()
        {
            var tasks = db.Tasks.Include(t => t.project).Include(t => t.skill);
            return View(Task.order(tasks));
        }

        //
        // GET: /Task/Details/5

        public virtual ViewResult Details(int id)
        {
            Task task = db.Tasks.Find(id);
            return View(task);
        }

        //
        // GET: /Task/Create

        public virtual ActionResult Create()
        {
            ViewBag.projectId = new SelectList(db.Projects, "id", "name");
            ViewBag.skillId = new SelectList(db.Skills, "id", "name");
            return View();
        } 

        //
        // POST: /Task/Create

        [HttpPost]
        public virtual ActionResult Create(Task task)
        {
            if (ModelState.IsValid)
            {
                db.Tasks.Add(task);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.projectId = new SelectList(db.Projects, "id", "name", task.projectId);
            ViewBag.skillId = new SelectList(db.Skills, "id", "name", task.skillId);
            return View(task);
        }
        
        //
        // GET: /Task/Edit/5

        public virtual ActionResult Edit(int id)
        {
            Task task = db.Tasks.Find(id);
            ViewBag.projectId = new SelectList(db.Projects, "id", "name", task.projectId);
            ViewBag.skillId = new SelectList(db.Skills, "id", "name", task.skillId);
            return View(task);
        }

        //
        // POST: /Task/Edit/5

        [HttpPost]
        public virtual ActionResult Edit(Task task)
        {
            if (ModelState.IsValid)
            {
                db.Entry(task).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.projectId = new SelectList(db.Projects, "id", "name", task.projectId);
            ViewBag.skillId = new SelectList(db.Skills, "id", "name", task.skillId);
            return View(task);
        }

        //
        // GET: /Task/Delete/5

        public virtual ActionResult Delete(int id)
        {
            Task task = db.Tasks.Find(id);
            return View(task);
        }

        //
        // POST: /Task/Delete/5

        [HttpPost, ActionName("Delete")]
        public virtual ActionResult DeleteConfirmed(int id)
        {            
            Task task = db.Tasks.Find(id);
            db.Tasks.Remove(task);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}