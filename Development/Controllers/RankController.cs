﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Development.Models;

namespace Development.Controllers
{
    public partial class RankController : Controller
    {
        private DevelopmentContext db = new DevelopmentContext();

        //
        // GET: /Rank/

        public virtual ViewResult Index(int? employeePositionId = null)
        {
            var ranks = db.Ranks.Include(r => r.employeePosition);
            var rankList = ranks.ToList();
            foreach (var rank in rankList)
            {
                if (rank.employeePositionId == employeePositionId)
                {
                    rankList.Remove(rank);
                }
            }
            return View(rankList);
        }

        //
        // GET: /Rank/Details/5

        public virtual ViewResult Details(int id)
        {
            Rank rank = db.Ranks.Find(id);
            return View(rank);
        }

        //
        // GET: /Rank/Create

        public virtual ActionResult Create()
        {
            ViewBag.employeePositionId = new SelectList(db.EmployeePositions, "id", "name");
            return View();
        } 

        //
        // POST: /Rank/Create

        [HttpPost]
        public virtual ActionResult Create(Rank rank)
        {
            if (ModelState.IsValid)
            {
                db.Ranks.Add(rank);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.employeePositionId = new SelectList(db.EmployeePositions, "id", "name", rank.employeePositionId);
            return View(rank);
        }
        
        //
        // GET: /Rank/Edit/5

        public virtual ActionResult Edit(int id)
        {
            Rank rank = db.Ranks.Find(id);
            ViewBag.employeePositionId = new SelectList(db.EmployeePositions, "id", "name", rank.employeePositionId);
            return View(rank);
        }

        //
        // POST: /Rank/Edit/5

        [HttpPost]
        public virtual ActionResult Edit(Rank rank)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rank).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.employeePositionId = new SelectList(db.EmployeePositions, "id", "name", rank.employeePositionId);
            return View(rank);
        }

        //
        // GET: /Rank/Delete/5

        public virtual ActionResult Delete(int id)
        {
            Rank rank = db.Ranks.Find(id);
            return View(rank);
        }

        //
        // POST: /Rank/Delete/5

        [HttpPost, ActionName("Delete")]
        public virtual ActionResult DeleteConfirmed(int id)
        {            
            Rank rank = db.Ranks.Find(id);
            db.Ranks.Remove(rank);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}