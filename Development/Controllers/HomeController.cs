﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Development.Models;

namespace Development.Controllers
{
    public partial class HomeController : Controller
    {
        private DevelopmentContext db = new DevelopmentContext();

        [Authorize]
        public virtual ActionResult Index()
        {
            var model = db.Employees.SingleOrDefault(x => x.username == User.Identity.Name);
            return View(model);
        }

        public virtual ActionResult About()
        {
            return View();
        }
    }
}
