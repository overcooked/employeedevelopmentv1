﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Development.Models;

namespace Development.Controllers
{
    public partial class SkillScoreController : Controller
    {
        private DevelopmentContext db = new DevelopmentContext();

        //
        // GET: /SkillScore/

        public virtual ViewResult Index()
        {
            return View(db.SkillScores.ToList());
        }

        //
        // GET: /SkillScore/Details/5

        public virtual ViewResult Details(int id)
        {
            SkillScore skillscore = db.SkillScores.Find(id);
            return View(skillscore);
        }

        //
        // GET: /SkillScore/Create

        public virtual ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /SkillScore/Create

        [HttpPost]
        public virtual ActionResult Create(SkillScore skillscore)
        {
            if (ModelState.IsValid)
            {
                db.SkillScores.Add(skillscore);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(skillscore);
        }
        
        //
        // GET: /SkillScore/Edit/5

        public virtual ActionResult Edit(int id)
        {
            SkillScore skillscore = db.SkillScores.Find(id);
            return View(skillscore);
        }

        //
        // POST: /SkillScore/Edit/5

        [HttpPost]
        public virtual ActionResult Edit(SkillScore skillscore)
        {
            if (ModelState.IsValid)
            {
                db.Entry(skillscore).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(skillscore);
        }

        //
        // GET: /SkillScore/Delete/5

        public virtual ActionResult Delete(int id)
        {
            SkillScore skillscore = db.SkillScores.Find(id);
            return View(skillscore);
        }

        //
        // POST: /SkillScore/Delete/5

        [HttpPost, ActionName("Delete")]
        public virtual ActionResult DeleteConfirmed(int id)
        {            
            SkillScore skillscore = db.SkillScores.Find(id);
            db.SkillScores.Remove(skillscore);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}