﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Development.Models;

namespace Development.Controllers
{
    public partial class LogController : Controller
    {
        private DevelopmentContext db = new DevelopmentContext();

        //
        // GET: /Log/

        [Authorize]
        public virtual ViewResult Index()
        {
            var empId = db.Employees.Where(x => x.username == User.Identity.Name).Select(x => x.id).SingleOrDefault();
            var model = new LogViewModel
            {
                Logs = db.Logs.Where(s => s.employee.id.Equals(empId))
                    .Include(l => l.employee).Include(l => l.skillLevel).Include(l => l.skill).Include(l => l.verifiedBy),
                Presenter = new PresenterViewModel(db)
            };

            model.TotalHours = model.Logs.Sum(x => x.hours).Value;
            model.PercentageRework = model.TotalHours > 0 ? model.Logs.Sum(x => x.hours * x.percentRework).Value / model.TotalHours : 0;
            model.PercentageCocaching = model.TotalHours > 0 ? model.Logs.Sum(x => x.hours * x.percentCoaching).Value / model.TotalHours : 0;

            return View(model);
        }

        //
        // GET: /Log/Special

        public virtual ViewResult Special(string employeeId, string date)
        {
            System.Linq.IQueryable<Development.Models.Log> logs;
            //The following section chooses the logs relating to the employee and date in an efficient way.
            if (!String.IsNullOrEmpty(employeeId))
            {
                int employeeIdInt = Convert.ToInt32(employeeId);
                if (!String.IsNullOrEmpty(date))
                {
                    var dateProper = Convert.ToDateTime(date);
                    logs = db.Logs.Where(s => s.date.Equals(dateProper) && s.employee.id.Equals(employeeIdInt))
                        .Include(l => l.employee).Include(l => l.skillLevel).Include(l => l.skill).Include(l => l.verifiedBy);
                }
                else
                {
                    logs = db.Logs.Where(s => s.employee.id.Equals(employeeIdInt))
                        .Include(l => l.employee).Include(l => l.skillLevel).Include(l => l.skill).Include(l => l.verifiedBy);
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(date))
                {
                    var dateProper = Convert.ToDateTime(date);
                    logs = db.Logs.Where(s => s.date.Equals(dateProper))
                        .Include(l => l.employee).Include(l => l.skillLevel).Include(l => l.skill).Include(l => l.verifiedBy);
                }
                else
                {
                    logs = db.Logs.Include(l => l.employee).Include(l => l.skillLevel).Include(l => l.skill).Include(l => l.verifiedBy);
                }
            }

            decimal totalHours = 0M;
            decimal rework = 0M;
            decimal coaching = 0M;
            foreach (var log in logs)
            {
                totalHours += (decimal)log.hours;
                rework += (decimal)(log.hours * log.percentRework);
                coaching += (decimal)(log.hours * log.percentCoaching);
            }
            ViewBag.totalHours = totalHours;
            if (totalHours != 0)
            {
                ViewBag.percentRework = rework / totalHours;
                ViewBag.percentCoaching = coaching / totalHours;
            }


            ViewBag.employeeId = new SelectList(db.Employees, "id", "firstName");
            //ViewBag.date = Convert.ToString(DateTime.Today);
            return View(logs.ToList());
        }

        //
        // GET: /Log/Details/5

        public virtual ViewResult Details(int id)
        {
            Log log = db.Logs.Find(id);
            return View(log);
        }

        //
        // GET: /Log/Create

        public virtual ActionResult Create()
        {
            var employeeIdInt = db.Employees.Where(x => x.username == User.Identity.Name).Select(x => x.id).SingleOrDefault();

            var model = new LogViewModel
            {
                Logs = db.Logs.Where(s => s.employee.id.Equals(employeeIdInt))
                    .Include(l => l.employee).Include(l => l.skillLevel).Include(l => l.skill).Include(l => l.verifiedBy),
                Presenter = new PresenterViewModel(db)
            };

            return View(model);
        } 

        //
        // POST: /Log/Create

        [HttpPost]
        public virtual ActionResult AddHours(FormCollection formCollection)
        {

            var employeeId = db.Employees.Where(x => x.username == User.Identity.Name).Select(x => x.id).SingleOrDefault();

            var rows = int.Parse(formCollection["hdnRows"]);

            for (var i = 1; i <= rows; i++)
            {
                var log = new Log
                {
                    date = DateTime.Parse(formCollection["date"]),
                    employeeId = employeeId,
                    skillId = int.Parse(formCollection[String.Format("skill{0}", i)]),
                    skillLevelId = int.Parse(formCollection[String.Format("skilllevel{0}", i)]),

                    hours = ReturnDecimal(formCollection[String.Format("hour{0}", i)]),

                    unitsCompleted = ReturnIntOrNull(formCollection[String.Format("unit{0}", i)]),
                    percentRework = ReturnIntOrNull(formCollection[String.Format("rework{0}", i)]),
                    percentCoaching = ReturnIntOrNull(formCollection[String.Format("coaching{0}", i)]),

                    notes = formCollection[String.Format("note{0}", i)]
                };

                db.Logs.Add(log);
            }

            db.SaveChanges();

            var model = new LogViewModel
            {
                Logs = db.Logs.Where(s => s.employee.id.Equals(employeeId))
                    .Include(l => l.employee).Include(l => l.skillLevel).Include(l => l.skill).Include(l => l.verifiedBy),
                Presenter = new PresenterViewModel(db)
            };

            model.TotalHours = model.Logs.Sum(x => x.hours).Value;
            model.PercentageRework = model.TotalHours > 0 ? model.Logs.Sum(x => x.hours * x.percentRework).Value / model.TotalHours : 0;
            model.PercentageCocaching = model.TotalHours > 0 ? model.Logs.Sum(x => x.hours * x.percentCoaching).Value / model.TotalHours : 0;

            return PartialView(MVC.Log.Actions.Views.HoursPartial, model);

        }

        private int? ReturnIntOrNull(string formvalue)
        {
            if (String.IsNullOrEmpty(formvalue)) return null;

            int result;
            int.TryParse(formvalue, out result);
            return result;
            
        }

        private decimal ReturnDecimal(string formvalue)
        {
            decimal result;
            decimal.TryParse(formvalue, out result);
            return result;
        }

        //
        // GET: /Log/Edit/5

        public virtual ActionResult Edit(int id)
        {
            Log log = db.Logs.Find(id);
            ViewBag.employeeId = new SelectList(db.Employees, "id", "firstName", log.employeeId);
            ViewBag.skillLevelId = new SelectList(db.SkillLevels, "id", "name", log.skillLevelId);
            ViewBag.skillId = new SelectList(db.Skills, "id", "name", log.skillId);
            ViewBag.verifiedById = new SelectList(db.Employees, "id", "firstName", log.verifiedById);
            return View(log);
        }

        //
        // POST: /Log/Edit/5

        [HttpPost]
        public virtual ActionResult Edit(Log log)
        {
            if (ModelState.IsValid)
            {
                db.Entry(log).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.employeeId = new SelectList(db.Employees, "id", "firstName", log.employeeId);
            ViewBag.skillLevelId = new SelectList(db.SkillLevels, "id", "name", log.skillLevelId);
            ViewBag.skillId = new SelectList(db.Skills, "id", "name", log.skillId);
            ViewBag.verifiedById = new SelectList(db.Employees, "id", "firstName", log.verifiedById);
            return View(log);
        }


        //
        // GET: /Log/Delete/5

        public virtual ActionResult Delete(int id)
        {
            Log log = db.Logs.Find(id);
            return View(log);
        }

        //
        // POST: /Log/Delete/5

        [HttpPost, ActionName("Delete")]
        public virtual ActionResult DeleteConfirmed(int id)
        {            
            Log log = db.Logs.Find(id);
            db.Logs.Remove(log);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public virtual ActionResult GetUnitsCompletedState(int id)
        {
            var skill = db.Skills.SingleOrDefault(x => x.id == id);
            var state =  UnitCompletedState.Optional;

            if (skill.holisitic != null && skill.individualistic != null)
            {
                if (!skill.holisitic.Value && skill.individualistic.Value)
                {
                    state = UnitCompletedState.Required;
                }
                if (skill.holisitic.Value && !skill.individualistic.Value)
                {
                    state = UnitCompletedState.Disabled;
                }
            }

            var skillLevels =
                skill.skillLevelEntries.Select(x => new SelectListItem {Text = x.skillLevel.description, Value = x.skillLevel.id.ToString()});
            
            var result = new
                        {
                            SkillLevels = skillLevels,
                            State = state,
                            Unit = skill.unitOfMeasurement
                        };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public virtual PartialViewResult Search(DateTime date, int id)
        {
            return PartialView();
        }



        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}