﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Development.Models;

namespace Development.Controllers
{
    public partial class AchievedRankController : Controller
    {
        private DevelopmentContext db = new DevelopmentContext();

        //
        // GET: /AchievedRank/

        public virtual ViewResult Index()
        {
            var achievedranks = db.AchievedRanks.Include(a => a.employee).Include(a => a.rank).Include(a => a.approvedBy).Include(a => a.verifiedBy);
            return View(achievedranks.ToList());
        }

        //
        // GET: /AchievedRank/Details/5

        public virtual ViewResult Details(int id)
        {
            AchievedRank achievedrank = db.AchievedRanks.Find(id);
            return View(achievedrank);
        }

        //
        // GET: /AchievedRank/Create

        public virtual ActionResult Create()
        {
            ViewBag.employeeId = new SelectList(db.Employees, "id", "firstName");
            ViewBag.rankId = new SelectList(db.Ranks, "id", "name");
            ViewBag.approvedById = new SelectList(db.Employees, "id", "firstName");
            ViewBag.verifiedById = new SelectList(db.Employees, "id", "firstName");
            return View();
        } 

        //
        // POST: /AchievedRank/Create

        [HttpPost]
        public virtual ActionResult Create(AchievedRank achievedrank)
        {
            if (ModelState.IsValid)
            {
                db.AchievedRanks.Add(achievedrank);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.employeeId = new SelectList(db.Employees, "id", "firstName", achievedrank.employeeId);
            ViewBag.rankId = new SelectList(db.Ranks, "id", "name", achievedrank.rankId);
            ViewBag.approvedById = new SelectList(db.Employees, "id", "firstName", achievedrank.approvedById);
            ViewBag.verifiedById = new SelectList(db.Employees, "id", "firstName", achievedrank.verifiedById);
            return View(achievedrank);
        }
        
        //
        // GET: /AchievedRank/Edit/5

        public virtual ActionResult Edit(int id)
        {
            AchievedRank achievedrank = db.AchievedRanks.Find(id);
            ViewBag.employeeId = new SelectList(db.Employees, "id", "firstName", achievedrank.employeeId);
            ViewBag.rankId = new SelectList(db.Ranks, "id", "name", achievedrank.rankId);
            ViewBag.approvedById = new SelectList(db.Employees, "id", "firstName", achievedrank.approvedById);
            ViewBag.verifiedById = new SelectList(db.Employees, "id", "firstName", achievedrank.verifiedById);
            return View(achievedrank);
        }

        //
        // POST: /AchievedRank/Edit/5

        [HttpPost]
        public virtual ActionResult Edit(AchievedRank achievedrank)
        {
            if (ModelState.IsValid)
            {
                db.Entry(achievedrank).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.employeeId = new SelectList(db.Employees, "id", "firstName", achievedrank.employeeId);
            ViewBag.rankId = new SelectList(db.Ranks, "id", "name", achievedrank.rankId);
            ViewBag.approvedById = new SelectList(db.Employees, "id", "firstName", achievedrank.approvedById);
            ViewBag.verifiedById = new SelectList(db.Employees, "id", "firstName", achievedrank.verifiedById);
            return View(achievedrank);
        }

        //
        // GET: /AchievedRank/Delete/5

        public virtual ActionResult Delete(int id)
        {
            AchievedRank achievedrank = db.AchievedRanks.Find(id);
            return View(achievedrank);
        }

        //
        // POST: /AchievedRank/Delete/5

        [HttpPost, ActionName("Delete")]
        public virtual ActionResult DeleteConfirmed(int id)
        {            
            AchievedRank achievedrank = db.AchievedRanks.Find(id);
            db.AchievedRanks.Remove(achievedrank);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}