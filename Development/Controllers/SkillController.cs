﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Development.Models;

namespace Development.Controllers
{
    public partial class SkillController : Controller
    {
        private DevelopmentContext db = new DevelopmentContext();

        //
        // GET: /Skill/

        public virtual ViewResult Index()
        {
            var skills = db.Skills.OrderBy(s => s.orderId).Include(s => s.parent);
            return View(Skill.order(skills));
        }

        //
        // GET: /Skill/Details/5

        public virtual ViewResult Details(int id)
        {
            Skill skill = db.Skills.Find(id);
            return View(skill);
        }

        //
        // GET: /Skill/Create

        public virtual ActionResult Create()
        {
            ViewBag.parentId = new SelectList(db.Skills, "id", "name");
            return View();
        } 

        //
        // POST: /Skill/Create

        [HttpPost]
        public virtual ActionResult Create(Skill skill)
        {
            if (ModelState.IsValid)
            {
                db.Skills.Add(skill);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.parentId = new SelectList(db.Skills, "id", "name", skill.parentId);
            return View(skill);
        }
        
        //
        // GET: /Skill/Edit/5

        public virtual ActionResult Edit(int id)
        {
            Skill skill = db.Skills.Find(id);
            ViewBag.parentId = new SelectList(db.Skills, "id", "name", skill.parentId);
            return View(skill);
        }

        //
        // POST: /Skill/Edit/5

        [HttpPost]
        public virtual ActionResult Edit(Skill skill)
        {
            if (ModelState.IsValid)
            {
                db.Entry(skill).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.parentId = new SelectList(db.Skills, "id", "name", skill.parentId);
            return View(skill);
        }

        //
        // GET: /Skill/Delete/5

        public virtual ActionResult Delete(int id)
        {
            Skill skill = db.Skills.Find(id);
            return View(skill);
        }

        //
        // POST: /Skill/Delete/5

        [HttpPost, ActionName("Delete")]
        public virtual ActionResult DeleteConfirmed(int id)
        {            
            Skill skill = db.Skills.Find(id);
            db.Skills.Remove(skill);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}