﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.Entity;
using Development.Models;
using Development.Models.DAL;
using Development.Services;
using Ninject.Modules;
using Development.EmployeeMembership;
using Ninject;
using System.Web.Security;
using Ninject.Web.Common;
using System.Reflection;
using Development.BLL;

namespace Development
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    internal class MyNinjectModules : NinjectModule
    {
        public override void Load()
        {
            Bind<IEmployeeMembershipProvider>()
                .To<EmployeeMembershipProvider>();

            Bind<ILogHoursService>()
                .To<LogHoursService>();

            Bind<IEmployeeService>()
                .To<EmployeeService>();

            Bind<IVerificationService>()
                .To<VerificationService>();

            Bind<IRankAchievementService>()
                .To<RankAchievementService>();

            Bind<ISkillAchievementService>()
                .To<SkillAchievementService>();

            Bind<IPresenterService>()
                .To<PresenterService>();

            Bind<IDevelopmentContext>()
                .To<DevelopmentContext>()
                .InRequestScope();

        }
    }

    public class MvcApplication : NinjectHttpApplication
    {
        private IKernel _kernel = new StandardKernel(new MyNinjectModules());

        protected override IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Load(new MyNinjectModules());
            return kernel;
        }

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected override void OnApplicationStarted()
        {
            //Database.SetInitializer<DevelopmentContext>(new DevelopmentInitializer());

            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            base.OnApplicationStarted();
        } 
    }
}