﻿using System.Linq;
using Development.Models;
using Development.Services;

namespace Development.BLL
{
    /// <summary>
    /// Employee Service
    /// </summary>
    public class EmployeeService : IEmployeeService
    {
        IDevelopmentContext db;

        public EmployeeService(IDevelopmentContext developmentContext)
        {
            db = developmentContext;
        }


        public int GetEmployeeId(string name)
        {
            return db.Employees.Where(x => x.username == name).Select(x => x.id).SingleOrDefault();
        }


        public int GetDefaultProjectId(int employeeId)
        {
            return db.Employees.Where(x => x.id == employeeId).Select(x => x.defaultProjectId).SingleOrDefault() ?? 0;
        }
    }
}