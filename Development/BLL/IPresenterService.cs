﻿using System.Web.Mvc;

namespace Development.BLL
{
    public interface IPresenterService
    {
        SelectList GetEmployees();
        SelectList GetProjects(int projectId);
        SelectList GetSkills();
        SelectList GetSkillLevels();
        SelectList GetTasks(int projectId);
        SelectList GetSkillScores();
    }
}