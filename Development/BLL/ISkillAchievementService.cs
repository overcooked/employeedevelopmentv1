﻿using System.Collections.Generic;
using Development.Models;

namespace Development.BLL
{
    public interface ISkillAchievementService
    {
        IList<AchievedSkill> GetPreviousAchievedSkills(int employeeId, int skillId, int skillLevelId);
    }
}