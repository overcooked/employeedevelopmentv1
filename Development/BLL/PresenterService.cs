﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Development.Models;
using Development.Services;

namespace Development.BLL
{
    /// <summary>
    /// Presenter Service used to populate various dropdowns.
    /// </summary>
    public class PresenterService : IPresenterService
    {

        readonly IDevelopmentContext db;
        
        public PresenterService(IDevelopmentContext developmentContext)
        {
            db = developmentContext;
        }

        public SelectList GetEmployees()
        {
            return new SelectList(Employee.order(db.Employees), "id", "fullName");
        }

        public SelectList GetProjects(int projectId)
        {
            return new SelectList(Project.order(db.Projects), "id", "name", projectId);
        }

        public SelectList GetSkills()
        {
            return new SelectList(Skill.order(db.Skills), "id", "indentedName");
        }

        public SelectList GetSkillLevels()
        {
            return new SelectList(db.SkillLevels.ToList(), "id", "name");
        }

        public SelectList GetTasks(int projectId)
        {
            return new SelectList(db.Tasks.Where(x => x.projectId == projectId), "id", "name");
        }

        public SelectList GetSkillScores()
        {
            return new SelectList(db.SkillScores.ToList(), "id", "name");
        }
    }
}