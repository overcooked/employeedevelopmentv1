﻿using System;
using System.Collections.Generic;
using Development.Models;
using Development.ViewModels;

namespace Development.BLL
{
    public interface IVerificationService
    {
        IEnumerable<VerifyHoursModel> GetAllHours(DateTime? dateFrom = null, DateTime? dateTo = null);
        IEnumerable<Log> GetNonVerifiedLogHours(int employeeId);
        IEnumerable<Log> GetNonVerifiedLogHours(int employeeId, DateTime dateFrom, DateTime dateTo);
        IEnumerable<VerifyHoursModel> GetNonVerifiedHours();
        IEnumerable<VerifyHoursModel> GetNonVerifiedHours(VerifyHoursSearch model);
        IEnumerable<VerifyHoursModel> GetVerifiedHours();
        IList<Log> GetHours(int id, DateTime? dateFrom, DateTime? dateTo, VerificationSearchType search);
    }

}