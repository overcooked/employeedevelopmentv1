﻿using System;
namespace Development.BLL
{
    public interface IEmployeeService
    {
        int GetEmployeeId(string name);

        int GetDefaultProjectId(int employeeId);
    }
}
