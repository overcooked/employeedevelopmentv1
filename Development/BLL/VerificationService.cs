﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Development.Models;
using Development.Services;
using Development.ViewModels;

namespace Development.BLL
{
    public class VerificationService : IVerificationService
    {
        readonly IDevelopmentContext db;
        private readonly ILogHoursService _logHoursService;

        public VerificationService(IDevelopmentContext developmentContext, ILogHoursService logHoursService)
        {
            db = developmentContext;
            _logHoursService = logHoursService;
        }
        
        public IEnumerable<VerifyHoursModel> GetAllHours(DateTime? dateFrom = null, DateTime? dateTo = null)
         {
             dateFrom = dateFrom ?? new DateTime(1900, 1, 1);
             dateTo = dateTo ?? new DateTime(3000, 1, 1);

             var seperateHours = (from logHours in db.Logs
                                  where logHours.date >= dateFrom.Value && logHours.date <= dateTo.Value
                                  group logHours by new { logHours.employeeId, logHours.employee.lastName, logHours.employee.firstName, logHours.IsVerified, logHours.project }
                                      into g
                                      select new
                                      {
                                          EmployeeFirstName = g.Key.firstName,
                                          EmployeeSurname = g.Key.lastName,
                                          EmployeeId = g.Key.employeeId,
                                          IsVerified = g.Key.IsVerified,
                                          TotalHours = g.Sum(x => x.hours),
                                          Project = g.Key.project.name
                                      });

             var allHours = new List<VerifyHoursModel>();

             foreach (var h in seperateHours)
             {
                 if (allHours.Any(x => x.EmployeeId == h.EmployeeId))
                 {
                     var hour = allHours.SingleOrDefault(x => x.EmployeeId == h.EmployeeId);
                     if (h.IsVerified)
                     {
                         hour.TotalVerifiedHours = h.TotalHours;
                     }
                     else
                     {
                         hour.TotalNonVerifiedHours = h.TotalHours;
                     }
                 }
                 else
                 {
                     if (h.IsVerified)
                     {
                         allHours.Add(new VerifyHoursModel
                         {
                             EmployeeId = h.EmployeeId,
                             EmployeeFirstName = h.EmployeeFirstName,
                             EmployeeSurname = h.EmployeeSurname,
                             IsVerified = h.IsVerified,
                             TotalVerifiedHours = h.TotalHours,
                             TotalNonVerifiedHours = 0,
                             DateFrom = dateFrom,
                             DateTo = dateTo,
                             ProjectName = h.Project
                         });
                     }
                     else
                     {
                         allHours.Add(new VerifyHoursModel
                         {
                             EmployeeId = h.EmployeeId,
                             EmployeeFirstName = h.EmployeeFirstName,
                             EmployeeSurname = h.EmployeeSurname,
                             IsVerified = h.IsVerified,
                             TotalNonVerifiedHours = h.TotalHours,
                             TotalVerifiedHours = 0,
                             DateFrom = dateFrom,
                             DateTo = dateTo,
                             ProjectName = h.Project
                         });
                     }
                 }
             }

             var totalReworkCoach = from logHours in db.Logs
                                    where logHours.date >= dateFrom.Value && logHours.date <= dateTo.Value
                                    group logHours by new { logHours.employeeId }
                                        into g
                                        select new
                                        {
                                            EmployeeId = g.Key.employeeId,
                                            Rework = g.Sum(x => (x.hours * x.percentRework)),
                                            Coach = g.Sum(x => (x.hours * x.percentCoaching)),
                                        };

             foreach (var log in allHours)
             {
                 var z = totalReworkCoach.SingleOrDefault(x => x.EmployeeId == log.EmployeeId).Rework / (log.TotalVerifiedHours + log.TotalNonVerifiedHours);
                 var y = totalReworkCoach.SingleOrDefault(x => x.EmployeeId == log.EmployeeId).Coach /
                                             (log.TotalVerifiedHours + log.TotalNonVerifiedHours);

                 if (z != null) log.TotalRework = Math.Round(z.Value);
                 if (y != null) log.TotalCoach = Math.Round(y.Value);
             }

             return allHours;
         }

        public IList<Log> GetHours(int id, DateTime? dateFrom, DateTime? dateTo, VerificationSearchType search)
        {
            dateFrom = dateFrom ?? new DateTime(1900, 1, 1);
            dateTo = dateTo ?? new DateTime(3000, 1, 1);

            return search != VerificationSearchType.Both
                       ? _logHoursService.GetLogHours(id).Logs.Where(
                           x =>
                           x.date >= dateFrom.Value && dateFrom <= dateTo.Value &&
                           x.IsVerified == (search == VerificationSearchType.VerifiedHours)).ToList()
                       : _logHoursService.GetLogHours(id).Logs.Where(x => x.date >= dateFrom && dateFrom <= dateTo).
                             ToList();
        }

        public IEnumerable<Log> GetNonVerifiedLogHours(int employeeId)
        {
            return db.Logs.Where(x => x.employeeId == employeeId && x.verifiedById == null).ToList();
        }

        public IEnumerable<Log> GetNonVerifiedLogHours(int employeeId, DateTime dateFrom, DateTime dateTo)
        {
            return db.Logs.Where(x =>
                                    x.employeeId == employeeId &&
                                    x.verifiedById == null &&
                                    x.date >= dateFrom &&
                                    x.date <= dateTo).ToList();
        }

        public IEnumerable<VerifyHoursModel> GetNonVerifiedHours()
        {
            return (from logHours in db.Logs
                    where logHours.verifiedById == null
                    group logHours by new { logHours.employeeId, logHours.employee.lastName, logHours.employee.firstName }
                        into g
                        select new VerifyHoursModel
                        {
                            EmployeeFirstName = g.Key.firstName,
                            EmployeeSurname = g.Key.lastName,
                            EmployeeId = g.Key.employeeId,
                            TotalNonVerifiedHours = g.Sum(x => x.hours)
                        });
        }

        public IEnumerable<VerifyHoursModel> GetNonVerifiedHours(VerifyHoursSearch model)
        {
            return (from logHours in db.Logs
                    where logHours.verifiedById == null && logHours.date >= model.DateFrom
                          && logHours.date <= model.DateTo &&
                          logHours.employee.employeePositionId == (int)model.EmployeeSearchType
                    group logHours by new { logHours.employeeId, logHours.employee.lastName, logHours.employee.firstName }
                        into g
                        select new VerifyHoursModel
                        {
                            EmployeeFirstName = g.Key.firstName,
                            EmployeeSurname = g.Key.lastName,
                            EmployeeId = g.Key.employeeId,
                            TotalNonVerifiedHours = g.Sum(x => x.hours)
                        });
        }

        public IEnumerable<VerifyHoursModel> GetVerifiedHours()
        {
            return (from logHours in db.Logs
                    where logHours.verifiedById != null
                    group logHours by new { logHours.employeeId, logHours.employee.lastName, logHours.employee.firstName }
                        into g
                        select new VerifyHoursModel
                        {
                            EmployeeFirstName = g.Key.firstName,
                            EmployeeSurname = g.Key.lastName,
                            EmployeeId = g.Key.employeeId,
                            TotalNonVerifiedHours = g.Sum(x => x.hours)
                        });
        }

    }
}