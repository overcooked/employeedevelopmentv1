﻿$(document).ready(function () {

    $('#checkAll').click(function () { $(':checkbox').attr('checked', 'checked'); });

    $('#unCheckAll').click(function () { $(':checkbox').removeAttr('checked'); });

    $("#toggleHours").click(function () { $("#addHours").toggle(); });

    $(".clonedInput").delegate(".skill", "change", function () {
        var cSelected = $(this).val();
        $.ajax({
            context: this,
            url: "/EmployeeMenu/GetLogHoursState",
            type: "POST",
            dataType: "json",
            data: ({ id: cSelected }),
            success: function (result) {
                var ddlId = $(this).attr('name');
                var idSlice = ddlId.slice(ddlId.length - 1, ddlId.length);

                $("#unitLabel" + idSlice).text("");

                var skillLevelDdl = $("#skilllevel" + idSlice);
                skillLevelDdl.removeAttr('disabled');
                //skillLevelDdl.empty();
                skillLevelDdl.children('option').remove();

                skillLevelDdl.append(
                        $('<option></option>').val("").text("Select")
                    );

                $.each(result.SkillLevels, function (Value, Skill) {
                    skillLevelDdl.append(
                        $('<option></option>').val(Skill.Value).text(Skill.Text)
                    );
                });

                switch (result.State) {
                    case 0: //Optional
                        $("#unit" + idSlice).removeAttr('class');
                        $("#unit" + idSlice).removeAttr('disabled');
                        $("#unit" + idSlice).attr('class', 'units input-small digits');
                        //Set Unit of measurement
                        $("#unitLabel" + idSlice).text(result.Unit);
                        break;
                    case 1: //Required
                        $("#unit" + idSlice).removeAttr('class');
                        $("#unit" + idSlice).removeAttr('disabled');
                        $("#unit" + idSlice).attr('class', 'units input-small required digits');
                        //Set Unit of measurement
                        $("#unitLabel" + idSlice).text(result.Unit);
                        break;
                    case 2: //Disabled
                        $("#unit" + idSlice).attr('disabled', 'disabled');
                    default:
                }
            }
        });
    });


    $('.datepicker').datepicker({
        showButtonPanel: false,
        changeMonth: true,
        changeYear: true,
        dateFormat: 'mm/dd/yy',
        yearRange: '-12:+0' // last 5 years

    });

    $('#btnAdd').click(function () {

        var num = $('.clonedInput').length; // how many "duplicatable" input fields we currently have  

        var newNum = new Number(num + 1);      // the numeric ID of the new input field being added  

        $('input[name=hdnRows]').val(newNum);

        // create the new element via clone(), and manipulate it's ID using newNum value                
        var newElem = $('#input' + num).clone(true).attr('id', 'input' + newNum);

        //Remove previous validation messages
        newElem.find("label.error").remove();

        // manipulate the name/id values of the input inside the new element                
        newElem.find('.skill').attr('id', 'skill' + newNum).attr('name', 'skill' + newNum);
        newElem.find('.hours').attr('id', 'hour' + newNum).attr('name', 'hour' + newNum).val('');
        newElem.find('.units').attr('id', 'unit' + newNum).attr('name', 'unit' + newNum).removeAttr("disabled").val('');
        newElem.find('.unitLabel').attr('id', 'unitLabel' + newNum).text('');
        newElem.find('.skilllevels').attr('id', 'skilllevel' + newNum).attr('name', 'skilllevel' + newNum).attr('disabled', 'disabled').empty().append($('<option></option>').text("Select"));
        newElem.find('.rework').attr('id', 'rework' + newNum).attr('name', 'rework' + newNum).val('');
        newElem.find('.coaching').attr('id', 'coaching' + newNum).attr('name', 'coaching' + newNum).val('');
        newElem.find('.notes').attr('id', 'note' + newNum).attr('name', 'note' + newNum).val('');

        // insert the new element after the last "duplicatable" input field                
        $('#input' + num).after(newElem);

        // enable the "remove" button                
        $('#btnDel').removeAttr("disabled");

        // business rule: you can only add 5 names                
        if (newNum == 5)
            $('#btnAdd').attr('disabled', 'disabled');
    });

    $('#btnDel').click(function () {
        var num = $('.clonedInput').length; // how many "duplicatable" input fields we currently have   
        $('input[name=hdnRows]').val(num - 1);
        $('#input' + num).remove();     // remove the last element                 
        // enable the "add" button                
        $('#btnAdd').removeAttr("disabled");
        // if only one element remains, disable the "remove" button                
        if (num - 1 == 1)
            $('#btnDel').attr('disabled', 'disabled');
    });

    $('.helpPopover').popover('hide');

    $('#btnDel').attr('disabled', 'disabled');

});