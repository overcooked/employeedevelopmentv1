﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Development.Models;

namespace Development.EmployeeMembership
{
    public interface IEmployeeMembershipProvider
    {
        bool ValidateUser(string username, string password);

        void CreateUser(Employee employee, out System.Web.Security.MembershipCreateStatus status);
    }
}
