﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Development.Models;
using System.Web.Security;

namespace Development.EmployeeMembership
{
    public class EmployeeMembershipProvider: IEmployeeMembershipProvider
    {
        private DevelopmentContext db = new DevelopmentContext();

        public bool ValidateUser(string username, string password)
        {
            return db.Employees.Any(x => x.username == username && x.password == password);
        }

        public void CreateUser(Employee employee, out System.Web.Security.MembershipCreateStatus status)
        {
            db.Employees.Add(employee);
            db.SaveChanges();
            status = System.Web.Security.MembershipCreateStatus.Success;
        }
    }
}