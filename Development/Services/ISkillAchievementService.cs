﻿using System.Collections.Generic;
using Development.Models;
using Development.ViewModels;

namespace Development.Services
{
    public interface ISkillAchievementService
    {
        IList<AchievedSkill> GetPreviousAchievedSkills(int employeeId, int skillId, int skillLevelId);

        decimal? GetHoursSinceLastAchievement(SkillAchievementViewModel model);

        ICollection<Log> GetAssociatedLogs(SkillAchievementViewModel model);

        bool HasLogHoursForAchievedSkill(AchievedSkill achievedSkill);

        decimal? GetCurrentAbility(SkillAchievementViewModel model);

        decimal? GetEfficencyScore(SkillAchievementViewModel model, bool isSixMonths = false);

        void AddAchievedSkill(AchievedSkill achievedSkill);

        int Save();
    }
}