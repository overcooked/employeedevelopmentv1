﻿using System.Web.Mvc;

namespace Development.Services
{
    public interface IPresenterService
    {
        SelectList GetEmployees();
        SelectList GetProjects(int projectId);
        SelectList GetSkills();
        SelectList GetSkillLevels();
        SelectList GetTasks(int projectId);
        SelectList GetSkillScores();
    }
}