﻿using System;
using System.Collections.Generic;
using Development.Models;
using Development.ViewModels;

namespace Development.Services
{
    public interface ILogHoursService
    {
        void AddLogHours(Development.Models.Log log);

        LogViewModel GetLogHours(int employeeId);

        object GetLogHoursState(int skillId);

        int Save();

        void Update(Log log);

        Log GetLog(int logId);
    }
}
