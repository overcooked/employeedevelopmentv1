﻿using System;
namespace Development.Services
{
    public interface IEmployeeService
    {
        int GetEmployeeId(string name);

        int GetDefaultProjectId(int employeeId);
    }
}
