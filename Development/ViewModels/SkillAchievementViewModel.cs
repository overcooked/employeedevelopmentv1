﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Development.Models;

namespace Development.ViewModels
{
    public class SkillAchievementViewModel
    {
        public AchievedSkill AchievedSkill { get; set; }
        public IList<AchievedSkill> PreviousSkills { get; set; }
        public PresenterViewModel Presenter { get; set; }
    }
}