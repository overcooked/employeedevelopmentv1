﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Development.ViewModels
{
    public class VerifyHoursViewModel
    {
        public IList<VerifyHoursModel> Hours { get; set; }

        public decimal? TotalVerifiedHours { get; set; }
        public decimal? TotalNonVerifiedHours { get; set; }
        public decimal? TotalHours { get; set; }

        public VerifyHoursSearch VerifyHoursSearch { get; set; }
    }
}