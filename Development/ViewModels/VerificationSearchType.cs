﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Development.ViewModels
{
    public enum VerificationSearchType
    {
        VerifiedHours,
        NonVerifiedHours,
        Both
    }
}
