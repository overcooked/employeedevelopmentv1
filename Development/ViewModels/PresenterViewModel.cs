﻿using System.Web.Mvc;
using Development.BLL;
using Development.Services;

namespace Development.ViewModels
{
    public class PresenterViewModel
    {
        public PresenterViewModel(IPresenterService presenterService, int projectId = 0)
        {
            Employees = presenterService.GetEmployees();
            Projects = presenterService.GetProjects(projectId);
            Skills = presenterService.GetSkills();
            SkillLevels = presenterService.GetSkillLevels();
            Tasks = presenterService.GetTasks(projectId);
            SkillScores = presenterService.GetSkillScores();
        }

        public SelectList Employees { get; set; }
        public SelectList Projects { get; set; }
        public SelectList Skills { get; set; }
        public SelectList SkillLevels { get; set; }
        public SelectList Tasks { get; set; }
        public SelectList SkillScores { get; set; }
    }
}
