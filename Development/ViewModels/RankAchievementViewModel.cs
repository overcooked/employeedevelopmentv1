﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Development.ViewModels
{
    public class RankAchievementViewModel
    {
        public PresenterViewModel Presenter { get; set; }

        [Display(Name = "Employee")]
        [Required]
        public int EmployeeId { get; set; }

        [Display(Name = "Skill Level")]
        [Required]
        public int SkillLevelId { get; set; }
    }
}
