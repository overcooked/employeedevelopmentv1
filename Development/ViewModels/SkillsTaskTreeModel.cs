﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Development.Models;

namespace Development.ViewModels
{
    public class SkillsTaskTreeModel
    {
        public List<SkillTasksModel> SkillTasksTree { get; set; }

        public SelectList SkillsSelector { get; set; }
    }


    public class SkillTasksModel
    {
        public Skill SkillNode { get; set; }
        public List<Task> SkillTasks { get; set; }
    }
}