﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Development.Helpers
{
    public class DevelopmentHelper
    {
        public static int? ReturnIntOrNull(string formvalue)
        {
            if (String.IsNullOrEmpty(formvalue)) return null;

            int result;
            int.TryParse(formvalue, out result);
            return result;

        }

        public static decimal ReturnDecimal(string formvalue)
        {
            decimal result;
            decimal.TryParse(formvalue, out result);
            return result;
        }
    }
}