﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Development.Models
{
    public class SkillLevelEntry
    {
        public int id { get; set; }
        public string description { get; set; }
        public decimal? numOfUnitsToMaster { get; set; }
        public decimal? avgHoursToMaster { get; set; }
        //[Key, Column(Order = 0)]
        public int skillId { get; set; }
        //[Key, Column(Order = 1)]
        public int skillLevelId { get; set; }
        public virtual Skill skill { get; set; }
        public virtual SkillLevel skillLevel { get; set; }
        public virtual ICollection<AchievedSkill> achievedSkills { get; set; }
    }
}