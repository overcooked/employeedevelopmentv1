﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Development.Models
{
    public enum EmployeeRole
    {
        Employee,
        EmployeeWithDirects,
        TrustedEmployee,
        Superintendent,
        Admin
    }
}
