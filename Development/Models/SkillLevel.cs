﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Development.Models
{
    public class SkillLevel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int order { get; set; }
        public virtual ICollection<SkillLevelEntry> skill_level_entries { get; set; }
        public virtual ICollection<Log> logs { get; set; }
        public virtual ICollection<RankRule> rank_rules { get; set; }
    }
}