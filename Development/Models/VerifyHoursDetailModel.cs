﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Development.Models
{
    public class VerifyHoursDetailModel
    {
        public IList<Log> Logs { get; set; }
        public Boolean IsVerified { get; set; }
    }
}