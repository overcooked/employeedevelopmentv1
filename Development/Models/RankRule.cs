﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Development.Models
{
    public class RankRule
    {
        public int id { get; set; }
        public decimal minScorePercentage { get; set; }
        public int order { get; set; }
        public int skillLevelId { get; set; }
        public int rankId { get; set; }
        public int skillId { get; set; }
        public virtual SkillLevel skillLevel { get; set; }
        public virtual Rank rank { get; set; }
        public virtual Skill skill { get; set; }
    }
}