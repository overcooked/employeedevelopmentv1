﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Development.Models
{
    public class Skill
    {
        public int id { get; set; }
        public string name { get; set; }
        public string unitOfMeasurement { get; set; }
        public Boolean? individualistic { get; set; }
        public Boolean? holisitic { get; set; }
        public string description { get; set; }
        public int orderId { get; set; }
        public int? parentId { get; set; }
        [ForeignKey("parentId")]
        public virtual Skill parent { get; set; }
        public virtual ICollection<SkillLevelEntry> skillLevelEntries { get; set; }
        public virtual ICollection<Task> tasks { get; set; }
        public virtual ICollection<Log> logs { get; set; }
        public virtual ICollection<RankRule> rankRules { get; set; }
        public virtual ICollection<Skill> children { get; set; }

        public int? topLevelSkillId { get; set; }
        public virtual Skill topLevelSkill { get; set; }

        public string indentedName
        {
            get
            {
                string padding = String.Concat(Enumerable.Repeat("--", numOfAncestors()));
                return padding + name;
            }
        }

        /// <summary>
        /// This method counts the number of ancestors (parent, grandparent, great grandparent, etc.).
        /// </summary>
        /// <returns>The total number of ancestors.</returns>
        public int numOfAncestors()
        {
            int i;
            var skill = this;
            for (i = 0; skill.parent != null; i++ )
            {
                skill = skill.parent;
            }
            return i;
        }

        /// <summary>
        /// This method orders skills based on a hierarchy of the parent-child relationships.  It will look like this:
        /// (GrandParent1, Parent1, Child1, 2ndParent1 GrandParent2, Parent2, GrandParent3, Parent3, Child3, 2ndChild3)
        /// </summary>
        /// <param name="skills">This is the skills that you want sorted.</param>
        /// <returns>Returns an ordered list of skills.</returns>
        public static List<Skill> order(IQueryable<Skill> skills)
        {
            return order(skills.ToList());
        }

        /// <summary>
        /// This method orders skills based on a hierarchy of the parent-child relationships.  It will look like this:
        /// (GrandParent1, Parent1, Child1, 2ndParent1 GrandParent2, Parent2, GrandParent3, Parent3, Child3, 2ndChild3)
        /// </summary>
        /// <param name="skills">This is the skills that you want sorted.</param>
        /// <returns>Returns an ordered list of skills.</returns>
        public static List<Skill> order(List<Skill> skills)
        {
            //First order all the skills by the order field.
            var skillsList = skills.OrderBy(s => s.orderId).ToList();

            //Create a new list with just the highest level parent skills.
            var orderedSkills = skillsList.Where(s => s.parentId == null).ToList();
            skillsList.RemoveAll(s => s.parentId == null);

            //Insert the children of the current parent in the correct position.
            for (var i = 0; skillsList.Count > 0; i++ )
            {
                var skill = orderedSkills.ElementAt(i);
                orderedSkills.InsertRange(orderedSkills.IndexOf(skill) + 1, skillsList.Where(s => s.parentId == skill.id));
                skillsList.RemoveAll(s => s.parentId == skill.id);
            }

            return orderedSkills;
        }
    }

    public enum UnitCompletedState
    {
        Optional,
        Required,
        Disabled
    }
}