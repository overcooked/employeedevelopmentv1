﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Development.Models
{
    public class Verification
    {
        public int id { get; set; }
        public DateTime date { get; set; }
        public decimal unitsCompleted { get; set; }
        public string notes { get; set; }
        public int taskId { get; set; }
        public int verifiedById { get; set; }
        public virtual Task task { get; set; }
        [ForeignKey("verifiedById")]
        public virtual Employee verifiedBy { get; set; }
    }
}