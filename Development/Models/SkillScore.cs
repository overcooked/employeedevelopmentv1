﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Development.Models
{
    public class SkillScore
    {
        public int id { get; set; }
        public string name { get; set; }
        public decimal scoreValue { get; set; }
        public decimal minEfficiency { get; set; }
        public decimal minAbility { get; set; }
        public string description { get; set; }
        public int order { get; set; }
        public virtual ICollection<AchievedSkill> achieved_skills { get; set; }
    }
}