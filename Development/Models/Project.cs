﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Development.Models
{
    public class Project
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime expEndDate { get; set; }
        public string description { get; set; }
        public int orderId { get; set; }
        public virtual ICollection<Task> tasks { get; set; }

        public static List<Project> order(IEnumerable<Project> projects)
        {
            return order(projects.ToList());
        }

        public static List<Project> order(List<Project> projects)
        {
            return projects.OrderBy(s => s.orderId).ToList();
        }
    }
}