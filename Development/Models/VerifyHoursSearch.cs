﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Development.Models
{
    public class VerifyHoursSearch
    {
        [Required]
        public DateTime DateFrom { get; set; }

        [Required]
        public DateTime DateTo { get; set; }

        //[Required]
        public EmployeeSearchType EmployeeSearchType { get; set; }

        
        public VerificationSearchType VerificationSearchType { get; set; }
    }
 
}
