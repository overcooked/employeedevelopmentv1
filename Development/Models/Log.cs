﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Development.Models
{
    public class Log
    {
        public int id { get; set; }
        
        [Display(Name = "Date")]
        [DisplayFormat(DataFormatString = "{0:d}")] 
        public DateTime date { get; set; }

        [Display(Name = "Hours")]
        public decimal? hours { get; set; }

        [Display(Name = "Units Completed")]
        public decimal? unitsCompleted { get; set; }

        [Display(Name = "Percent Rework")]
        public int? percentRework { get; set; }

        [Display(Name = "Percent Coaching")]
        public int? percentCoaching { get; set; }

        [Display(Name = "Notes")]
        public string notes { get; set; }

        [Display(Name = "Employee")]
        public int employeeId { get; set; }

        [Display(Name = "Skill Level")]
        public int skillLevelId { get; set; }

        [Display(Name = "Skill")]
        public int skillId { get; set; }

        public bool IsVerified { get; set; }

        public int? verifiedById { get; set; }

        public decimal? verifiedUnitsCompletd { get; set; }

        public decimal? verifiedHours { get; set; }

        public int projectId { get; set; }

        [ForeignKey("projectId")]
        public virtual Project project { get; set; }

        [ForeignKey("employeeId")]
        public virtual Employee employee { get; set; }

        [Display(Name = "Skill Level")]
        public virtual SkillLevel skillLevel { get; set; }

        [Display(Name = "Skill")]
        public virtual Skill skill { get; set; }
        
        [ForeignKey("verifiedById")]
        public virtual Employee verifiedBy { get; set; }

        //[ForeignKey("achievedSkillId")]
        //public virtual AchievedSkill achievedSkill { get; set; }

        //public int achievedSkillId { get; set; }
    }
}