﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Development.Models
{
    public class EmployeePosition
    {
        public int id { get; set; }
        public string name { get; set; }
        public int minRank { get; set; }
        public string description { get; set; }
        public int order { get; set; }
        public virtual ICollection<Rank> ranks { get; set; }
    }
}