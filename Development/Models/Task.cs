﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Development.Models
{
    public class Task
    {
        public int id { get; set; }
        public decimal? scheduledHours { get; set; }
        public decimal? numOfUnits { get; set; }
        public string notes { get; set; }
        public int projectId { get; set; }
        public int skillId { get; set; }
        public virtual Project project { get; set; }
        public virtual Skill skill { get; set; }
        public virtual ICollection<Verification> verifications { get; set; }

        public string name
        {
            get
            {
                return this.skill.name;
            }
        }

        /// <summary>
        /// This method counts the number of ancestors (parent, grandparent, great grandparent, etc.).
        /// </summary>
        /// <returns>The total number of ancestors.</returns>
        public int numOfAncestors()
        {
            return this.skill.numOfAncestors();
        }

        /// <summary>
        /// Orders the Tasks according to how the related skills would be ordered.
        /// </summary>
        /// <param name="tasks">This is the tasks you want ordered.</param>
        /// <returns>Returns an ordered list of tasks.</returns>
        public static List<Task> order(IEnumerable<Task> tasks)
        {
            return order(tasks.ToList());
        }

        /// <summary>
        /// Orders the Tasks according to how the related skills would be ordered.
        /// </summary>
        /// <param name="tasks">This is the tasks you want ordered.</param>
        /// <returns>Returns an ordered list of tasks.</returns>
        public static List<Task> order(List<Task> tasks)
        {
            //First the related skills are sorted using the Skill order method.
            List<Skill> skills = new List<Skill>();
            foreach (var task in tasks)
            {
                skills.Add(task.skill);
            }
            skills = Skill.order(skills);

            //Then the tasks are ordered in the same way as the related skills were ordered.
            List<Task> orderedTasks = new List<Task>();
            foreach (var skill in skills)
            {
                foreach (var task in tasks)
                {
                    if (task.skill.Equals(skill))
                    {
                        orderedTasks.Add(task);
                        tasks.Remove(task);
                        break;
                    }
                }
            }

            return orderedTasks;
        }
    }
}