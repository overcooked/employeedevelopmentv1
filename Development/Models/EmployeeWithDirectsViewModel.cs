﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Development.Models
{
    public class EmployeeWithDirectsViewModel
    {
        public IEnumerable<AchievedSkill> AchievedSkills { get; set; }
        public PresenterViewModel Presenter { get; set; }
    }
}