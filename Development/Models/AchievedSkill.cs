﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Development.Models
{
    public class AchievedSkill
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime date { get; set; }
        public decimal? EfficencyScore { get; set; }
        public decimal estAbility { get; set; }
        public Boolean LearnedElsewhere { get; set; }
        public string Notes { get; set; }
        public int EmployeeId { get; set; }
        public int SkillId { get; set; }
        public int SkillLevelId { get; set; }
        public int? skillScoreId { get; set; }
        public int? ApprovedById { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual Employee employee { get; set; }
        public virtual Skill skill { get; set; }
        public virtual SkillLevel skillLevel { get; set; }
        public virtual SkillScore skillScore { get; set; }
        [ForeignKey("ApprovedById")]
        public virtual Employee ApprovedBy { get; set; }

        public virtual ICollection<Log> AssociatedLogs { get; set; }

        public decimal? HoursSinceLastSkillAchievement { get; set; }
        public decimal? PreviousAbility { get; set; }
        public decimal? CurrentAbility { get; set; }
        public decimal? SixMonthEfficencyScore { get; set; }
        public decimal? EfficencyScoreOverride { get; set; }
    }
}