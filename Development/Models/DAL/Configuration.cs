﻿using System.Data.Entity.Migrations;

namespace Development.Models.DAL
{
    public class Configuration : DbMigrationsConfiguration<DevelopmentContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
        }

        //protected override void Seed(DevelopmentContext context)
        //{
        //    var employeePositions = new List<EmployeePosition>
        //    {  
        //        new EmployeePosition { name = "General Laborer", minRank = 0, description = "Entry-Level", order = 1 },
        //        new EmployeePosition { name = "Skilled Laborer", minRank = 5, description = "Basic Skills Mastered", order = 2 },
        //        new EmployeePosition { name = "Team Lead", minRank = 10, description = "Leads Small Teams", order = 3 },
        //        new EmployeePosition { name = "Foreman", minRank = 15, description = "Leads Medium Teams", order = 4 },
        //        new EmployeePosition { name = "Superintendent", minRank = 20, description = "Leads Entire Crews", order = 5 },
        //        new EmployeePosition { name = "Project Manager", minRank = 25, description = "Leads Crews and Coordinates Subcontractors", order = 6 },
        //        new EmployeePosition { name = "Regional Manager", minRank = 30, description = "Leads a Division", order = 7 },
        //        new EmployeePosition { name = "Chief Executive", minRank = 35, description = "Leads the Company", order = 8 },
        //    };
        //    employeePositions.ForEach(s => context.EmployeePositions.AddOrUpdate(s));
        //    context.SaveChanges();
        //    var projects = new List<Project>
        //    {  
        //        new Project { name = "Mill Pond II Phase I", expEndDate = new DateTime(2011,9,1), description = "This is the first building.", orderId = 1 },
        //        new Project { name = "Mill Pond II Phase II", expEndDate = new DateTime(2012,6,1), description = "This is the second building.", orderId = 2 },
        //        new Project { name = "Mill Pond II Phase III", expEndDate = new DateTime(2013,5,1), description = "This is the third building.", orderId = 3 },
        //        new Project { name = "Mill Pond II Phase IV", expEndDate = new DateTime(2014,4,1), description = "This is the fourth building.", orderId = 4 },
        //    };
        //    projects.ForEach(s => context.Projects.AddOrUpdate(s));
        //    context.SaveChanges();
        //    var ranks = new List<Rank>
        //    {  
        //        new Rank { name = "+1 Rank - Beginner Core Step 1", value = 1, description = "Achieve a Beginner Core Skills Score of 15%", order = 1, employeePositionId = 1 },
        //        new Rank { name = "+1 Rank - Beginner Core Step 2", value = 1, description = "Achieve a Beginner Core Skills Score of 30%", order = 2, employeePositionId = 1 },
        //        new Rank { name = "+1 Rank - Beginner Core Step 3", value = 1, description = "Achieve a Beginner Core Skills Score of 45%", order = 3, employeePositionId = 1 },
        //        new Rank { name = "+1 Rank - Intermediate Core Step 1", value = 1, description = "Achieve a Beginner Core Skills Score of 60% and an Intermediate Core Skills Score of 10%", order = 4, employeePositionId = 1 },
        //        new Rank { name = "+1 Rank - Intermediate Core Step 2", value = 1, description = "Achieve a Beginner Core Skills Score of 75% and an Intermediate Core Skills Score of 20%", order = 5, employeePositionId = 1 },
        //        new Rank { name = "+1 Rank - Master The Beginner Core", value = 1, description = "Achieve a Beginner Core Skills Score of 90%", order = 1, employeePositionId = 2 },
        //        new Rank { name = "+2 Rank - Intermediate Core Step 3", value = 2, description = "Achieve an Intermediate Core Skills Score of 70%", order = 1, employeePositionId = 2 },
        //        new Rank { name = "+2 Rank - Beginner Manager Step 1", value = 2, description = "Achieve a Beginner Manager Skills Score of 30%", order = 1, employeePositionId = 2 },
        //    };
        //    ranks.ForEach(s => context.Ranks.AddOrUpdate(s));
        //    context.SaveChanges();
        //    var skillScores = new List<SkillScore>
        //    {
        //        new SkillScore { name = "Beginner", scoreValue = 0, minEfficiency = 0, minAbility = 0, description = "Entry Level", order = 1 },
        //        new SkillScore { name = "Good", scoreValue = 1, minEfficiency = 0.85M, minAbility = 0.5M, description = "The employee works at 85% of the scheduled speed and can complete 50% of the job with minimal supervision.", order = 2 },
        //        new SkillScore { name = "Great", scoreValue = 3, minEfficiency = 0.95M, minAbility = 0.75M, description = "The employee works at 95% of the scheduled speed and can complete 75% of the job with minimal supervision.", order = 3 },
        //        new SkillScore { name = "Awesome", scoreValue = 4, minEfficiency = 1.05M, minAbility = 0.95M, description = "The employee works at 105% of the scheduled speed and can complete 95% of the job with minimal supervision.", order = 4 },
        //        new SkillScore { name = "Outstanding", scoreValue = 5, minEfficiency = 1.15M, minAbility = 1.0M, description = "The employee works at 115% of the scheduled speed and can complete 100% of the job with minimal supervision.", order = 4 },
        //    };
        //    skillScores.ForEach(s => context.SkillScores.AddOrUpdate(s));
        //    context.SaveChanges();
        //    var employees = new List<Employee>
        //    {  
        //        new Employee { firstName = "John", lastName = "Smith", username = "johnsmith", password = "johnsmith", email = "kasoninc@gmail.com", employedSince = new DateTime(2010,4,1), hourlyPay = 11M, notes = "Rental & Construction Employee", defaultProjectId = 2, employeePositionId = 2, },
        //        new Employee { firstName = "Jane", lastName = "Johnson", username = "janejohnson", password = "janejohnson", email = "kasoninc@gmail.com", employedSince = new DateTime(2011,6,1), hourlyPay = 7.5M, notes = "Seasonal Employee", defaultProjectId = 2, employeePositionId = 1, supervisorId = 1 },
        //    };
        //    employees.ForEach(s => context.Employees.AddOrUpdate(s));
        //    context.SaveChanges();
        //    var achievedRanks = new List<AchievedRank>
        //    {  
        //        new AchievedRank { date = new DateTime(2012, 7, 1), notes = "Flawlessly Achieved", employeeId = 2, rankId = 1, approvedById = 1 },
        //        new AchievedRank { date = new DateTime(2012, 8, 1), notes = "Just barely passed.", employeeId = 2, rankId = 2, approvedById = 1 },
        //    };
        //    achievedRanks.ForEach(s => context.AchievedRanks.AddOrUpdate(s));
        //    context.SaveChanges();
        //    var skills = new List<Skill>
        //    {  
        //        //new Skill { name = "Footings", unitOfMeasurement = "Footing Volume (Cubic Feet)", description = "Creating the footing.",  orderId = 1 },
        //        //new Skill { name = "Backfill Around Forms", 
        //        //    unitOfMeasurement = "Linear Feet of Form Backfilled", description = "Backfilling",  
        //        //    orderId = 2 , parentId = 1, individualistic = false, holisitic = false},
        //        //new Skill { name = "Skill3", unitOfMeasurement = "measure", description = "Skill3",  orderId = 1 , parentId = 1, individualistic = false, holisitic = true},
        //        //new Skill { name = "Skill4", unitOfMeasurement = "measure", description = "Skill4",  orderId = 1 , parentId = 2, individualistic = true, holisitic = false},
        //        //new Skill { name = "Skill5", unitOfMeasurement = "measure", description = "Skill5",  orderId = 2 , parentId = 2, individualistic = true, holisitic = true},
        //        //new Skill { name = "Grandparent", unitOfMeasurement = "measure", description = "Grandparent",  orderId = 2 , individualistic = true, holisitic = true},
        //        //new Skill { name = "Parent", unitOfMeasurement = "measure", description = "Parent",  orderId = 1 , parentId = 6, individualistic = false, holisitic = true},
        //        //new Skill { name = "Child", unitOfMeasurement = "measure", description = "Child",  orderId = 2 , parentId = 6, individualistic = true, holisitic = true}


        //        //new Skill { name = "Skill_1", unitOfMeasurement = "Footing Volume (Cubic Feet)", description = "Skill_1",  orderId = 1, topLevelSkillId = 1},
        //        //    new Skill { name = "Skill_1.1", unitOfMeasurement = "Linear", description = "Skill_1.1", orderId = 1 , parentId = 1, individualistic = false, holisitic = false, topLevelSkillId = 1},
        //        //        new Skill { name = "Skill_1.1.1", unitOfMeasurement = "measure", description = "Skill_1.1.1",  orderId = 1 , parentId = 2, individualistic = false, holisitic = true, topLevelSkillId = 1},
        //        //        new Skill { name = "Skill_1.1.2", unitOfMeasurement = "measure", description = "Skill_1.1.2",  orderId = 2 , parentId = 2, individualistic = true, holisitic = false, topLevelSkillId = 1},
        //        //    new Skill { name = "Skill_1.2", unitOfMeasurement = "measure", description = "Skill_1.2",  orderId = 2 , parentId = 1, individualistic = true, holisitic = true, topLevelSkillId = 1},
        //        //        new Skill { name = "Skill_1.2.1", unitOfMeasurement = "measure", description = "Skill_1.2.1",  orderId = 2 , parentId = 5,individualistic = true, holisitic = true, topLevelSkillId = 1},
        //        //new Skill { name = "Skill_2", unitOfMeasurement = "measure", description = "Skill_2",  orderId = 2 , individualistic = false, holisitic = true, topLevelSkillId = 7},
        //        //    new Skill { name = "Skill_2.1", unitOfMeasurement = "measure", description = "Skill_2.1",  orderId = 1 , parentId = 7, individualistic = true, holisitic = true, topLevelSkillId = 7},

        //        new Skill { name = "Core Construction Skills", description = "These are the core construction skills needed by any good construction worker.",  orderId = 1, topLevelSkillId = 1},
        //            new Skill { name = "Footings", unitOfMeasurement = "Feet", description = "Footings are the foundation for the entire building.", orderId = 1 , parentId = 1, individualistic = false, holisitic = false, topLevelSkillId = 1},
        //                new Skill { name = "Build Forms", unitOfMeasurement = "Feet", description = "Forms are the wood the holds the concrete in place while it sets.  This is measured by the length of forms installed.",  orderId = 1 , parentId = 2, individualistic = false, holisitic = false, topLevelSkillId = 1},
        //                new Skill { name = "Install Rebar", unitOfMeasurement = "Feet", description = "Metal rebar ties the concrete together.  This is measured by the length of rebar installed.",  orderId = 2 , parentId = 2, individualistic = true, holisitic = false, topLevelSkillId = 1},
        //                new Skill { name = "Pour Concrete", unitOfMeasurement = "Cubic Yards", description = "Concrete is what the footings is made out of.",  orderId = 3 , parentId = 2, individualistic = false, holisitic = true, topLevelSkillId = 1},
        //            new Skill { name = "Backfilling", unitOfMeasurement = "Feet", description = "Backfilling is installing the draintile and any subsuface piping and then backfilling the hole with dirt.",  orderId = 2, parentId = 1, individualistic = false, holisitic = false, topLevelSkillId = 1},
        //                new Skill { name = "Install Draintile", unitOfMeasurement = "Feet", description = "Install draintile, rock, and tar paper.  This is measured by the length of draintile installed.",  orderId = 1, parentId = 5, individualistic = false, holisitic = false, topLevelSkillId = 1},
        //                new Skill { name = "Backfill Dirt", unitOfMeasurement = "Feet", description = "This is measured by the number of feet around the building that has been backfilled.",  orderId = 2, parentId = 5, individualistic = false, holisitic = false, topLevelSkillId = 1},
        //        new Skill { name = "Manager Skills", description = "These are the skill necessary for a good manager to have.",  orderId = 2 , topLevelSkillId = 7},
        //            new Skill { name = "Relationships", unitOfMeasurement = "Count", description = "Relationships with coworkers is critical to a sucessful business.",  orderId = 1 , parentId = 7, individualistic = true, holisitic = false, topLevelSkillId = 7},
        //    };

        //    skills.ForEach(s => context.Skills.AddOrUpdate(s));
        //    context.SaveChanges();
        //    var skillLevels = new List<SkillLevel>
        //    {  
        //        new SkillLevel { name = "Beginner", description = "Entry-Level",  order = 1 },
        //        new SkillLevel { name = "Intermediate", description = "Managing Crews",  order = 2 },
        //        new SkillLevel { name = "Advanced", description = "$10,000+ Issues",  order = 3 },
        //    };
        //    skillLevels.ForEach(s => context.SkillLevels.AddOrUpdate(s));
        //    context.SaveChanges();
        //    var skillLevelEntries = new List<SkillLevelEntry>
        //    {  
        //        new SkillLevelEntry { description = "Assisted in installing and pouring footings.", skillId = 2, skillLevelId = 1 },
        //        new SkillLevelEntry { description = "Led a small crew in installing and pouring the footings.", skillId = 2, skillLevelId = 2 },
        //        new SkillLevelEntry { description = "Determined key positions like the building elevation.", skillId = 2, skillLevelId = 3 },
        //        new SkillLevelEntry { description = "Built forms given a preset level and string line.", skillId = 3, skillLevelId = 1 },
        //        new SkillLevelEntry { description = "Set level and string line for the forms.", skillId = 3, skillLevelId = 2 },
        //        new SkillLevelEntry { description = "Installed rebar according to the direction of a supervisor.", skillId = 4, skillLevelId = 1 },
        //        new SkillLevelEntry { description = "Checked rebar placement according to plans.", skillId = 4, skillLevelId = 2 },
        //        new SkillLevelEntry { description = "Poured and leveled concrete", skillId = 5, skillLevelId = 1 },
        //        new SkillLevelEntry { description = "Estimated volume of concrete needed and coordinated pumper truck.", skillId = 5, skillLevelId = 2 },
        //        new SkillLevelEntry { description = "Backfilled building according to supervisor's directions.", skillId = 6, skillLevelId = 1 },
        //        new SkillLevelEntry { description = "Backfilled building and installed draintile according to plan.", skillId = 6, skillLevelId = 2 },
        //        new SkillLevelEntry { description = "Cleaned footing, laid pipe, placed rock, and cut tar paper.", skillId = 7, skillLevelId = 1 },
        //        new SkillLevelEntry { description = "Backfilled dirt around the building.", skillId = 8, skillLevelId = 1 },
        //        new SkillLevelEntry { description = "Meet with different coworkers for lunch 20 times.", skillId = 10, skillLevelId = 1 },
        //        new SkillLevelEntry { description = "Conduct one-on-ones with all directs every week for 2 months without missing a single one-on-one.", skillId = 10, skillLevelId = 2 },
        //        new SkillLevelEntry { description = "Read a book, or watch a class about how to improve relationships.  Then master at least three of the techniques that you learned.", skillId = 10, skillLevelId = 3 },
        //    };
        //    skillLevelEntries.ForEach(s => context.SkillLevelEntries.AddOrUpdate(s));
        //    context.SaveChanges();
        //    var rankRules = new List<RankRule>
        //    {  
        //        new RankRule { minScorePercentage = 15, order = 1, skillLevelId = 1, rankId = 1, skillId = 1},
        //        new RankRule { minScorePercentage = 30, order = 1, skillLevelId = 1, rankId = 2, skillId = 1},
        //        new RankRule { minScorePercentage = 45, order = 1, skillLevelId = 1, rankId = 3, skillId = 1},
        //        new RankRule { minScorePercentage = 60, order = 1, skillLevelId = 1, rankId = 4, skillId = 1},
        //        new RankRule { minScorePercentage = 10, order = 1, skillLevelId = 2, rankId = 4, skillId = 1},
        //        new RankRule { minScorePercentage = 75, order = 1, skillLevelId = 1, rankId = 5, skillId = 1},
        //        new RankRule { minScorePercentage = 20, order = 1, skillLevelId = 2, rankId = 5, skillId = 1},
        //        new RankRule { minScorePercentage = 90, order = 1, skillLevelId = 1, rankId = 6, skillId = 1},
        //        new RankRule { minScorePercentage = 70, order = 1, skillLevelId = 2, rankId = 7, skillId = 1},
        //        new RankRule { minScorePercentage = 30, order = 1, skillLevelId = 1, rankId = 8, skillId = 9},
        //        new RankRule { minScorePercentage = 50, order = 1, skillLevelId = 1, rankId = 1, skillId = 1},
        //        new RankRule { minScorePercentage = 30, order = 1, skillLevelId = 2, rankId = 2, skillId = 7},
        //        new RankRule { minScorePercentage = 25, order = 1, skillLevelId = 3, rankId = 2, skillId = 7},
        //    };
        //    rankRules.ForEach(s => context.RankRules.AddOrUpdate(s));
        //    context.SaveChanges();
        //    var tasks = new List<Task>
        //    {
        //        new Task { scheduledHours = 120M, numOfUnits = 400, notes = "The rain may slow up this task.", projectId = 1, skillId = 2},
        //        new Task { scheduledHours = 80M, numOfUnits = 350, notes = "A sand shoot may help make this go faster.", projectId = 1, skillId = 6},
        //        new Task { scheduledHours = 150M, numOfUnits = 500, notes = "This estimate is accurate if there is minimal rain fall.", projectId = 2, skillId = 2},
        //        new Task { scheduledHours = 100M, numOfUnits = 450, notes = "This is pretty accurate.", projectId = 2, skillId = 6},
        //        new Task { scheduledHours = 80M, numOfUnits = 450, notes = "The crew should not forget the tar paper.", projectId = 2, skillId = 7},
        //        new Task { scheduledHours = 30M, numOfUnits = 450, notes = "I hope this task gets done faster than 30 man-hours.", projectId = 2, skillId = 8},
        //        new Task { scheduledHours = 130M, numOfUnits = 450, notes = "This went too slowly on Mill Pond II Phase II.", projectId = 3, skillId = 2},
        //        new Task { scheduledHours = 90M, numOfUnits = 400, notes = "This estimate is more of a guess.", projectId = 3, skillId = 6},
        //    };
        //    tasks.ForEach(s => context.Tasks.AddOrUpdate(s));
        //    context.SaveChanges();
        //    var verifications = new List<Verification>
        //    {  
        //        new Verification { date = new DateTime(2012, 3, 13), unitsCompleted = 32, notes = "This is a low estimate.", taskId = 1, verifiedById = 1},
        //        new Verification { date = new DateTime(2012, 3, 13), unitsCompleted = 12, notes = "It was hard to estimate.", taskId = 2, verifiedById = 1},
        //    };
        //    verifications.ForEach(s => context.Verifications.AddOrUpdate(s));
        //    context.SaveChanges();
        //    var achievedSkills = new List<AchievedSkill>
        //    {  
        //        new AchievedSkill { date = new DateTime(2012, 3, 16), EfficencyScore = .9M, estAbility = .6M, LearnedElsewhere = false, Notes = "Unsure about this skill.", EmployeeId = 2, SkillId = 3, SkillLevelId = 1, skillScoreId = 3, ApprovedById = 1},
        //        new AchievedSkill { date = new DateTime(2012, 5, 20), EfficencyScore = .95M, estAbility = .8M, LearnedElsewhere = false, Notes = "Earned it.", EmployeeId = 2, SkillId = 5, SkillLevelId = 1, skillScoreId = 5, ApprovedById = 1},
        //        new AchievedSkill { date = new DateTime(2012, 3, 16), EfficencyScore = .9M, estAbility = .6M, LearnedElsewhere = false, Notes = "Unsure about this skill.", EmployeeId = 2, SkillId = 6, SkillLevelId = 1, skillScoreId = 2, ApprovedById = 1},
        //        new AchievedSkill { date = new DateTime(2012, 5, 20), EfficencyScore = .95M, estAbility = .8M, LearnedElsewhere = false, Notes = "Earned it.", EmployeeId = 2, SkillId = 8, SkillLevelId = 2, skillScoreId = 4, ApprovedById = 1},
        //    };
        //    achievedSkills.ForEach(s => context.AchievedSkills.AddOrUpdate(s));
        //    context.SaveChanges();
        //    var logs = new List<Log>
        //    {  
        //        new Log { date = new DateTime(2012, 3, 15), hours = 4.5M, unitsCompleted = 5.2M, percentRework = 0, percentCoaching = 0, notes = "This was tough.", employeeId = 2, skillLevelId = 1, skillId = 1, projectId = 1},
        //        new Log { date = new DateTime(2012, 1, 11), hours = 2M, unitsCompleted = 100M, percentRework = 0, percentCoaching = 0, notes = "I did some rework, but it was hard to estimate.", employeeId = 2, skillLevelId = 1, skillId = 1, projectId = 1},
        //    };
        //    logs.ForEach(s => context.Logs.AddOrUpdate(s));
        //    context.SaveChanges();
        //}
    }
}