﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Development.Models
{
    public class LogViewModel
    {
        public IEnumerable<Log> Logs { get; set; }
        public Log LogToAdd { get; set; }
        public PresenterViewModel Presenter { get; set; }
        public decimal TotalHours { get; set; }

        [DisplayFormat(DataFormatString = "{0:0.0}", ApplyFormatInEditMode = true)] 
        public decimal PercentageRework { get; set; }

        [DisplayFormat(DataFormatString = "{0:0.0}",ApplyFormatInEditMode = true)] 
        public decimal PercentageCocaching { get; set; }
    }
}