﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace Development.Models
{
    public class Rank
    {
        public int id { get; set; }
        public string name { get; set; }
        public int value { get; set; }
        public string description { get; set; }
        public int order { get; set; }
        public int employeePositionId { get; set; }
        public virtual EmployeePosition employeePosition { get; set; }
        public virtual ICollection<RankRule> rank_rules { get; set; }
        //public virtual ICollection<AchievedRank> achieved_ranks { get; set; }
    }
}