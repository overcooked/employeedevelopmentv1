﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Development.Models
{
    public class VerifyHoursModel
    {
        public int EmployeeId { get; set; }
        public string EmployeeFirstName { get; set; }
        public string EmployeeSurname { get; set; }
        public decimal? TotalNonVerifiedHours { get; set; }
        public decimal? TotalVerifiedHours { get; set; }
        public bool IsVerified { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public String ProjectName { get; set; }
        public decimal? TotalRework { get; set; }
        public decimal? TotalCoach { get; set; }
    }

}