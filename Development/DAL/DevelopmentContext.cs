﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using Development.Models.DAL;

namespace Development.Models
{
    public interface IDevelopmentContext
    {
        DbSet<AchievedRank> AchievedRanks { get; set; }
        DbSet<AchievedSkill> AchievedSkills { get; set; }
        DbSet<Employee> Employees { get; set; }
        DbSet<EmployeePosition> EmployeePositions { get; set; }
        DbSet<Log> Logs { get; set; }
        DbSet<Project> Projects { get; set; }
        DbSet<Rank> Ranks { get; set; }
        DbSet<RankRule> RankRules { get; set; }
        DbSet<Skill> Skills { get; set; }
        DbSet<SkillLevel> SkillLevels { get; set; }
        DbSet<SkillLevelEntry> SkillLevelEntries { get; set; }
        DbSet<SkillScore> SkillScores { get; set; }
        DbSet<Task> Tasks { get; set; }
        DbSet<Verification> Verifications { get; set; }
        Database Database { get; }
        DbChangeTracker ChangeTracker { get; }
        DbContextConfiguration Configuration { get; }
        int SaveChanges();
        IEnumerable<DbEntityValidationResult> GetValidationErrors();
        DbEntityEntry Entry(object entity);
        void Dispose();
        string ToString();
        bool Equals(object obj);
        int GetHashCode();
        Type GetType();
    }

    public class DevelopmentContext : DbContext, IDevelopmentContext
    {
        public DbSet<AchievedRank> AchievedRanks { get; set; }
        public DbSet<AchievedSkill> AchievedSkills { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeePosition> EmployeePositions { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Rank> Ranks { get; set; }
        public DbSet<RankRule> RankRules { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<SkillLevel> SkillLevels { get; set; }
        public DbSet<SkillLevelEntry> SkillLevelEntries { get; set; }
        public DbSet<SkillScore> SkillScores { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Verification> Verifications { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DevelopmentContext, Configuration>());

            modelBuilder.Entity<Employee>()
                        .HasRequired(a => a.employeePosition)
                        .WithMany()
                        .HasForeignKey(u => u.employeePositionId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

        }
}
}