﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Development.Models;

namespace Development.Models.DAL
{
    //public class DevelopmentInitializer : DontDropDbJustCreateTablesIfModelChanged<DevelopmentContext>
    public class DevelopmentInitializer : DropCreateDatabaseAlways<DevelopmentContext>
    {
        protected override void Seed(DevelopmentContext context)
        {
            var employeePositions = new List<EmployeePosition>
            {  
                new EmployeePosition { name = "General Laborer", minRank = 0, description = "Entry-Level", order = 1 },
                new EmployeePosition { name = "Skilled Laborer", minRank = 5, description = "Basic Skills Mastered", order = 2 },
            };
            employeePositions.ForEach(s => context.EmployeePositions.Add(s));
            context.SaveChanges();
            var projects = new List<Project>
            {  
                new Project { name = "Mill Pond II Phase I", expEndDate = new DateTime(2011,9,1), description = "This is the first building.", orderId = 1 },
                new Project { name = "Mill Pond II Phase II", expEndDate = new DateTime(2012,6,1), description = "This is the second building.", orderId = 2 },
                new Project { name = "Mill Pond II Phase III", expEndDate = new DateTime(2013,5,1), description = "This is the third building.", orderId = 3 },
            };
            projects.ForEach(s => context.Projects.Add(s));
            context.SaveChanges();
            var ranks = new List<Rank>
            {  
                new Rank { name = "+1 Rank - A", value = 1, description = "Achieve a Beginner Core Skills Score of 5", order = 1, employeePositionId = 1 },
                new Rank { name = "+1 Rank - B", value = 1, description = "Achieve a Beginner Core Skills Score of 10", order = 2, employeePositionId = 1 },
            };
            ranks.ForEach(s => context.Ranks.Add(s));
            context.SaveChanges();
            var skillScores = new List<SkillScore>
            {
                new SkillScore { name = "Beginner", scoreValue = 0, minEfficiency = 0, minAbility = 0, description = "Entry Level", order = 1 },
                new SkillScore { name = "Good", scoreValue = 1, minEfficiency = 0.5M, minAbility = 0.85M, description = "Just starting to understand", order = 2 },
                new SkillScore { name = "Better", scoreValue = 3, minEfficiency = 0.5M, minAbility = 0.85M, description = "Just starting to understand", order = 3 },
                new SkillScore { name = "MuchBetter", scoreValue = 4, minEfficiency = 0.5M, minAbility = 0.85M, description = "Just starting to understand", order = 4 },
                new SkillScore { name = "Best", scoreValue = 5, minEfficiency = 0.5M, minAbility = 0.85M, description = "Just starting to understand", order = 4 },
            };
            skillScores.ForEach(s => context.SkillScores.Add(s));
            context.SaveChanges();
            var employees = new List<Employee>
            {  
                new Employee { firstName = "John", lastName = "Smith", username = "johnsmith", password = "johnsmith", email = "kasoninc@gmail.com", employedSince = new DateTime(2010,4,1), hourlyPay = 11M, notes = "Rental & Construction Employee", defaultProjectId = 2, employeePositionId = 2, },
                new Employee { firstName = "Jane", lastName = "Johnson", username = "janejohnson", password = "janejohnson", email = "kasoninc@gmail.com", employedSince = new DateTime(2011,6,1), hourlyPay = 7.5M, notes = "Seasonal Employee", defaultProjectId = 2, employeePositionId = 1, supervisorId = 1 },
            };
            employees.ForEach(s => context.Employees.Add(s));
            context.SaveChanges();
            var achievedRanks = new List<AchievedRank>
            {  
                new AchievedRank { date = new DateTime(2012, 7, 1), notes = "Flawlessly Achieved", employeeId = 2, rankId = 1, approvedById = 1 },
                new AchievedRank { date = new DateTime(2012, 8, 1), notes = "Just barely passed.", employeeId = 2, rankId = 2, approvedById = 1 },
            };
            achievedRanks.ForEach(s => context.AchievedRanks.Add(s));
            context.SaveChanges();
            var skills = new List<Skill>
            {  
                //new Skill { name = "Footings", unitOfMeasurement = "Footing Volume (Cubic Feet)", description = "Creating the footing.",  orderId = 1 },
                //new Skill { name = "Backfill Around Forms", 
                //    unitOfMeasurement = "Linear Feet of Form Backfilled", description = "Backfilling",  
                //    orderId = 2 , parentId = 1, individualistic = false, holisitic = false},
                //new Skill { name = "Skill3", unitOfMeasurement = "measure", description = "Skill3",  orderId = 1 , parentId = 1, individualistic = false, holisitic = true},
                //new Skill { name = "Skill4", unitOfMeasurement = "measure", description = "Skill4",  orderId = 1 , parentId = 2, individualistic = true, holisitic = false},
                //new Skill { name = "Skill5", unitOfMeasurement = "measure", description = "Skill5",  orderId = 2 , parentId = 2, individualistic = true, holisitic = true},
                //new Skill { name = "Grandparent", unitOfMeasurement = "measure", description = "Grandparent",  orderId = 2 , individualistic = true, holisitic = true},
                //new Skill { name = "Parent", unitOfMeasurement = "measure", description = "Parent",  orderId = 1 , parentId = 6, individualistic = false, holisitic = true},
                //new Skill { name = "Child", unitOfMeasurement = "measure", description = "Child",  orderId = 2 , parentId = 6, individualistic = true, holisitic = true}


                new Skill { name = "Skill_1", unitOfMeasurement = "Footing Volume (Cubic Feet)", description = "Skill_1",  orderId = 1, topLevelSkillId = 1},
                    new Skill { name = "Skill_1.1", unitOfMeasurement = "Linear", description = "Skill_1.1", orderId = 1 , parentId = 1, individualistic = false, holisitic = false, topLevelSkillId = 1},
                        new Skill { name = "Skill_1.1.1", unitOfMeasurement = "measure", description = "Skill_1.1.1",  orderId = 1 , parentId = 2, individualistic = false, holisitic = true, topLevelSkillId = 1},
                        new Skill { name = "Skill_1.1.2", unitOfMeasurement = "measure", description = "Skill_1.1.2",  orderId = 2 , parentId = 2, individualistic = true, holisitic = false, topLevelSkillId = 1},
                    new Skill { name = "Skill_1.2", unitOfMeasurement = "measure", description = "Skill_1.2",  orderId = 2 , parentId = 1, individualistic = true, holisitic = true, topLevelSkillId = 1},
                        new Skill { name = "Skill_1.2.1", unitOfMeasurement = "measure", description = "Skill_1.2.1",  orderId = 2 , parentId = 5,individualistic = true, holisitic = true, topLevelSkillId = 1},
                new Skill { name = "Skill_2", unitOfMeasurement = "measure", description = "Skill_2",  orderId = 2 , individualistic = false, holisitic = true, topLevelSkillId = 7},
                    new Skill { name = "Skill_2.1", unitOfMeasurement = "measure", description = "Skill_2.1",  orderId = 1 , parentId = 7, individualistic = true, holisitic = true, topLevelSkillId = 7}
            };

            skills.ForEach(s => context.Skills.Add(s));
            context.SaveChanges();
            var skillLevels = new List<SkillLevel>
            {  
                new SkillLevel { name = "Beginner", description = "Entry-Level",  order = 1 },
                new SkillLevel { name = "Intermediate", description = "Managing Crews",  order = 2 },
                new SkillLevel { name = "Advanced", description = "$10,000+ Issues",  order = 3 },
            };
            skillLevels.ForEach(s => context.SkillLevels.Add(s));
            context.SaveChanges();
            var skillLevelEntries = new List<SkillLevelEntry>
            {  
                new SkillLevelEntry { description = "Backfill and Pour", skillId = 1, skillLevelId = 1 },
                new SkillLevelEntry { description = "Set Forms and Lead Crew", skillId = 1, skillLevelId = 2 },
                new SkillLevelEntry { description = "Determine Building Elevation", skillId = 1, skillLevelId = 3 },
            };
            skillLevelEntries.ForEach(s => context.SkillLevelEntries.Add(s));
            context.SaveChanges();
            var rankRules = new List<RankRule>
            {  
                new RankRule { minScorePercentage = 50, order = 1, skillLevelId = 1, rankId = 1, skillId = 1},
                new RankRule { minScorePercentage = 25, order = 1, skillLevelId = 3, rankId = 2, skillId = 7},
            };
            rankRules.ForEach(s => context.RankRules.Add(s));
            context.SaveChanges();
            var tasks = new List<Task>
            {  
                new Task { scheduledHours = 15M, numOfUnits = 100, notes = "The rain may slow up this task.", projectId = 2, skillId = 1},
                new Task { scheduledHours = 10M, numOfUnits = 200, notes = "This is pretty accurate.", projectId = 2, skillId = 2},
            };
            tasks.ForEach(s => context.Tasks.Add(s));
            context.SaveChanges();
            var verifications = new List<Verification>
            {  
                new Verification { date = new DateTime(2012, 3, 13), unitsCompleted = 32, notes = "This is a low estimate.", taskId = 1, verifiedById = 1},
                new Verification { date = new DateTime(2012, 3, 13), unitsCompleted = 12, notes = "It was hard to estimate.", taskId = 2, verifiedById = 1},
            };
            verifications.ForEach(s => context.Verifications.Add(s));
            context.SaveChanges();
            var achievedSkills = new List<AchievedSkill>
            {  
                new AchievedSkill { date = new DateTime(2012, 3, 16), EfficencyScore = .9M, estAbility = .6M, LearnedElsewhere = false, Notes = "Unsure about this skill.", EmployeeId = 2, SkillId = 3, SkillLevelId = 1, skillScoreId = 3, ApprovedById = 1},
                new AchievedSkill { date = new DateTime(2012, 5, 20), EfficencyScore = .95M, estAbility = .8M, LearnedElsewhere = false, Notes = "Earned it.", EmployeeId = 2, SkillId = 5, SkillLevelId = 1, skillScoreId = 5, ApprovedById = 1},
                 new AchievedSkill { date = new DateTime(2012, 3, 16), EfficencyScore = .9M, estAbility = .6M, LearnedElsewhere = false, Notes = "Unsure about this skill.", EmployeeId = 2, SkillId = 6, SkillLevelId = 1, skillScoreId = 2, ApprovedById = 1},
                new AchievedSkill { date = new DateTime(2012, 5, 20), EfficencyScore = .95M, estAbility = .8M, LearnedElsewhere = false, Notes = "Earned it.", EmployeeId = 2, SkillId = 8, SkillLevelId = 2, skillScoreId = 4, ApprovedById = 1},
            };
            achievedSkills.ForEach(s => context.AchievedSkills.Add(s));
            context.SaveChanges();
            var logs = new List<Log>
            {  
                new Log { date = new DateTime(2012, 3, 15), hours = 4.5M, unitsCompleted = 5.2M, percentRework = 0, percentCoaching = 0, notes = "This was tough.", employeeId = 2, skillLevelId = 1, skillId = 1, projectId = 1},
                new Log { date = new DateTime(2012, 1, 11), hours = 2M, unitsCompleted = 100M, percentRework = 0, percentCoaching = 0, notes = "I did some rework, but it was hard to estimate.", employeeId = 2, skillLevelId = 1, skillId = 1, projectId = 1},
            };
            logs.ForEach(s => context.Logs.Add(s));
            context.SaveChanges();
        }
    }
}